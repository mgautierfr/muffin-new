menu-position: 3
title:   Présentation d'idée : PGPID
id: pgpid
css-template: article.css
color1: #3c6eb4
index-style: pure-u-1 pure-u-md-1-3
authors: GaMa
date:    2013-07-23T00:59:56+02:00
license: CC by-sa
tags:    reconnaissance
---


Bien le bonjour à toi journal (ainsi qu'à tes lecteurs),

Aujourd'hui, pour mon premier journal, je m'en vais vous présenter une idée qui me taraude depuis bien longtemps.
Vous vous en doutez, vous avez lu le titre, c'est de PGPID dont je vais vous parler.
Avant d'aller plus loin, je ne vais pas vous tromper sur la marchandise : Il y a pas de code, pas de soft à tester. C'est juste une idée jetée sur un clavier.


# Pourquoi PGPID #

On parle de plus en plus (ici en tout cas) de réseaux sociaux décentralisés.
Certains ne jurent que par eux (quid de la maitrise de ses données ?), d'autres sont plus circonspects (quid de la disponibilité ?).

Il existe de nombreuses solutions pour faire du réseau social plus ou moins décentralisé/a-centralisé/fédéré.
Les citer serait trop long, sans intérêt et ceux que j'oublierai se sentiraient exclus et se vexeraient. Alors, je ne le ferais pas :)

Ces solutions marchent bien mais, de mon point de vue, elles souffrent de deux problèmes communs :

- l'identification des utilisateurs.
- la localisation des machines fournissant un/des service(s) auprès des utilisateurs.

La plupart utilisent un identifiant d'utilisateur de la forme "utilisateur@fournisseur.tld" ou "fournisseur.tld/utilisateur"
Le problème de ces identifiants est qu'ils sont dépendant du fournisseur.
Ainsi :

- C'est le DNS du fournisseur qui permet de localiser la machine qui va bien.
- Pour deux services différents chez deux fournisseurs différents il y aura deux identifiants.
- Le jour où mon fournisseur ferme ou que je veux en changer, il me faut changer de fournisseur.

La solution couramment évoquée est d'avoir son propre nom de domaine. Honnêtement, j'y crois pas beaucoup. Malgré l'infinité de possibilité de nom de domaine, les domaines intéressants sont eux limités.
(Mon nom de famille est déjà pris par une société. Et même si j'aurai pu l'avoir il y a pas mal de monde qui a le même nom que moi).
Quant bien même, un tld intéressant serait disponible, il n'est pas donné d'en acheter un (vous voyez votre grand-mère aller sur un registar acheter "tante-Huguette.fr" ?).

Et les noms de domaine ont un sens. Les goûts et les couleurs changeant avec le temps, que faire de ce sens ? 
Tous ces problèmes m'amène à penser que les noms de domaine ne sont pas une solution à long terme
(ça marche bien pour un nombre limité d'utilisateurs, pour 6 milliards de personnes c'est plus problématique)

On pourra cependant noter [RetroShare](http://retroshare.sourceforge.net/) (le seul que je connaisse) qui utilise une solution relativement (voir très) proche de celle présentée ici.
Il utilise une clé PGP pour identifier un utilisateur et une DHT pour identifier les adresses IP des machines.
La solution utilisée par RetroShare me semble bonne, elle est par contre dépendante de RetroShare.
La solution que je propose reprend celle de RetroShare, mais sous un axe global et indépendant des applications.
Il ne devrait pas être problématique de porter RetroShare sur PGPID.

Je vous invite à lire le très bon article de Stéphane Bortzmeyer (qui traine aussi ici) au sujet des url  : http://www.bortzmeyer.org/no-free-lunch.html

# Présentation #

PGPID est donc une solution basée sur PGP, qui fournirait :

- un système entièrement a-centré d'identification et d'authentification des utilisateurs.
- un système d'association de machines à un utilisateur.
- un système bas niveau fournissant des services de base aux applications construites au-dessus.

Je vous préviens tout de suite, PGPID n'est pas une solution complète :
C'est une solution pour un réseau social au sens premier du terme. Il ne présume en rien des applications bâties dessus (même si j'en parle un peu).

C'est vous voulez un résumé en une phrase (et donc fausse) : PGPID est au réseau d'humain ce que IP/DNS est au réseau d'ordinateur.

Le principe de base est le suivant :

- L'utilisateur se crée une clé à chiffrement symétrique (PGP)
- La clé publique sert d'identifiant.
- Cet identifiant est mis dans une DHT.
- À un identifiant est associé un certain nombre de métadonnées dans la DHT.
- Pour éviter qu'un vilain pirate ne change vos données, l'ensemble des données est signé avec la clé associée.
- Une donnée principale (si ce n'est pas la seule donnée) est une liste de nœuds.
- Chacun de ces nœuds, lorsqu'ils sont interrogés, permettent de connaitre quel nœuds fournit quel services associés à la clé.

## Définitions ##

Bon, c'est très loin d'être une spec, mais il faut bien avoir des noms clairs pour savoir de quoi on parle.
J'en conviens c'est un peu imbuvable à première lecture, mais n'arrêtez pas tous de suite, il y a des exemples après.

### Service

Fonctionnalité apportée par une application (le plus souvent cliente).
Un service peut être :
    
- Un fournisseur d'adresse de machine et des services associés.
- Une application de messagerie instantanée.
- Un serveur web.
- Un service de notifications
- Un service d'échange de fichiers
- Un serveur mail.

### SN (Service Name)

Le nom d'un service.

### PGPID

L'identifiant d'un compte. C'est le fingerprint de la clé PGP.


### PGPIDC

PGPID content
    
La clé publique complète PGP.


### SPN (ou Node)

Service Provider Node.
    
Une machine fournissant un ou plusieurs services.

### NDS

Node discovery service
    
Un service particulier qui permet d'obtenir une liste de SPN associé à une PGPID.

### NDN

Node discovery node (Noeud d'aiguillage).
    
Un SPN fournissant NDS.
Ce sont ces nœuds qui l'on met dans la DHT.
Le NDS n'étant qu'un service particulier, le NDS du NDN doit inclure le NDN dans la liste.

### SDS

Service discovery service

Un service fournissant la liste des services associés à un PGPID fournit par le SPN.
Chaque SPN doit avoir un SDS

### TS

Table service
    
Service fournissant la partie DHT (stockage/recherche des clés)

### TN

Table node
    
Un SPN fournissant TS

### FPGPID

Full PGPID. Il est de la forme : \<PGP fingerprint\>[@\<SPN\>][:\<SN\>]
Il permet d'identifier jusqu'à un service particulier d'une machine particulière d'un utilisateur.

# Identification #

L'identifiant de l'utilisateur est le fingerprint de la clé PGP.

Chaque utilisateur peut potentiellement avoir plusieurs nœuds fournissant des services. Ces machines sont identifiées par : ID@SPN
Ici, SPN est un nom choisi arbitrairement par l'utilisateur. Ça peut être «maison», «pc», «portable», ...
Ceci peut être comparé au ressource dans les xmppid.

Chaque service à un nom, (plus ou moins standardisé). C'est l'équivalent des ports.
Ainsi un service mail pourra être adressé avec l'identifiant : ID@maison:mail
De même un service de messagerie instantané sur téléphone sera adressé avec : ID@phone:im


# Use Case #

## Inscription ##

Alice souhaite s'inscrire (si on peut appeler ça s'inscrire) sur PGPID.

1. Elle lance son programme et crée une paire de clés PGP.
1. Elle contacte un TN connu et lui envoie sa PGPIDC.
1. Le TN se charge de diffuser sa PGPIDC dans la DHT.

Alice a maintenant une «identité» sur PGPID

## Identification sur les sites web ##

Son navigateur web étant configuré, lorsque Alice navigue sur un site supportant l'identification PGPID, elle est automatiquement identifiée.

1. Échange d'information entre le navigateur et le serveur web pour s'informer qu'ils supportent l'identification PGPID
1. Le serveur envoie un défi au navigateur
1. Le navigateur chiffre le défi (avec éventuelle confirmation d'Alice)
1. Le serveur récupère le PGPIDC d'Alice en contactant un TN.
1. Le serveur vérifie le chiffrement du défi et Alice est authentifiée.

Aucun mot de passe ne transite et n'est fourni à aucun serveur.

## Participation au réseau PGPID ##

Alice souhaite participer au réseau en installant un TS sur sa machine.

1. Elle installe un TS sur sa machine.
1. Elle contacte un TN et enregistre sa machine en tant que nouveau TN.
1. L'adresse IP:port du TS sont propagés dans la DHT.
1. Son TN commence à recevoir des PGPIDC à stocké et des requêtes de PGPID.

## Installer et fournir de nouveaux services ##

Pour pouvoir fournir des services la machine d'Alice doit devenir un SPN

1. Alice installe un SPS. Sa machine devient donc un SPN
1. Pour que son SPN puisse être contactée Alice doit enregistrer son SPN auprès d'un NDN.
1. Elle décide d'utiliser le NDN de «la Société À la Mode sur Internet»
1. Elle contacte donc le NDN et y enregistre son SPS (adresse IP du SPN et port du SPS)
1. Alice rajoute l'adresse IP du NDN et de port du NDS aux métadonnées associées à son PGPID, les signes et les envoie sur un TN connu (probablement le sien)
1. Son SDS est configuré pour indiquer que le SPN fourni les services SDS (obligatoirement) et TS (puisque c'est aussi un TN)

## Installer un service de messagerie instantané (IM) ##

1. Alice a installé un service de messagerie supportant le protocole PGPID
1. Alice configure son SDS pour qu'il indique aussi que son SN fourni le service IM

## Communiquer avec Bob ##

1. Alice rentre le PGPID de Bob dans son logiciel IM
1. son logiciel contacte un TN, demande le PGPIDC de Bob et la liste de NDN.
1. le logiciel contacte le(s) NDN et demande au NDS une liste de SPN correspondant au PGPID de Bob
1. le logiciel contacte le(s) SPN et demande au SDS la liste des services fournit.
1. si le service IM est dans la liste, on a trouvé le SPN fournissant le service IM
1. le logiciel d'Alice contacte le logiciel de Bob sur le bon SPN
1. Une authentification a lieu (défi, chiffrement, déchiffrement)
1. La communication commence

## Avoir un deuxième SPN ##

Alice a un portable, elle souhaite installer un logiciel IM pour discuter à partir de celui ci

1. Alice installe un SPS sur son portable
1. Alice configure le NDS pour qu'il retourne aussi l'adresse de son portable.
1. Bob peut contacter Alice aux adresses :
  * \<Alice PGPID\>. Le SPN contacté dépendra de l'ordre de priorité configuré dans le NDS.
  * \<Alice PGPID\>@pc
  * \<Alice PGPID\>@portable
  
## Idées de service possible ##

* vcard. service fournissant des informations complémentaires sur l'utilisateur (adresse, avatar, ...). Le service vcard (comme tous les autres services, y compris NDS et SDS) peut fournir des informations différentes selon le demandeur.
* service web.
* réseaux sociaux
* échange de fichier
* sauvegarde de données. PGPID1@pc peut envoyer des données à PGPID2@pc/backup. Permet de sauvegarder automatiquement des données chez d'autres personnes et vice versa (échange de bon procédés)
* synchronisation de données entre les machines d'une même personne. (entre PGPID1@pc et PGPID1@portable)

# Utilisation avancée #

## Avoir plusieurs NDN ##

Alice a aussi un téléphone portable. Elle souhaite l'utiliser, ainsi que son pc, comme NDN. Le téléphone servant de NDN de secours lorsque son pc est éteint.
De plus elle souhaite aussi s'en servir pour faire de la IM.

Elle installe donc :

+ sur son pc
  - un NDS qui liste son pc.
  - un SDS qui liste les services disponibles sur son pc
+ sur son téléphone portable
  - un NDS qui liste son portable.
  - un SDS qui liste les services disponibles sur son portable (IM)
+ dans la DHT
  - l'adresse de ses deux NDS, le NDS du pc ayant une plus grande priorité.
  
## Itinérance ##

Bob a un accès internet avec une adresse IP dynamique.
Pour éviter que la DHT soit mise à jour trop souvent, Bob utilise un NDN ne lui appartenant pas qui lui a une adresse fixe.
Celui-ci sert de relais vers son adresse dynamique.
Son SPN contacte régulièrement les NDN qui le référencent pour qu'ils mettent à jour leurs informations. (équivalent des services dynDNS)
C'est d’ailleurs la raison d'être des NDN (au lieu de mettre directement les SPN dans la DHT) : Avoir des SPN dynamiques et utiliser les NDN (fixe) qui servent «d'aiguillage». Le tout sans remettre à jour la DHT trop souvent.

## Résilience des données ##

Alice a fait un blog. Pour cela elle utilise le service blog sur son pc.
Son PC n'est pas toujours connecté mais Alice souhaiterais que ses informations soient toujours disponibles.
Elle s'arrange avec Bob. Lorsque Alice écrit un article, elle le publie sur son pc mais aussi sur une machine de Bob.
Elle configure son NDN pour qu'il retourne le SPN de Bob.
Bob de son côté configure son SPN pour répondre aux requêtes au sujet d'Alice.
Lorsque Charles veut lire les articles d'Alice il se contacte à son NDN. Si le SPN d'Alice est connecté il va lire l'article dessus.
Sinon il se connecte au SPN de Bob pour accéder à l'article.
Les données venant d'Alice devant être signé par la clé privée d'Alice et Bob ne pouvant en aucun cas y accéder (à la clé privée), il faut qu'Alice signe ses articles à l'avance.
Il ne peut donc pas y avoir de contenue dynamique hébergé chez un tiers. (Sauf à mettre en place un protocole de délégation des signatures/réseau de confiance avancé)

Alice, Bob et Charles peuvent aussi se mettre d'accord pour louer un serveur et mettre en place un NDN/SPN commun.


# Conclusion #

Je pense que vous avez le compris le principe et je laisse votre imagination débordante trouver de nouveaux cas d'utilisation.

J'aimerais pourtant revenir sur quelques points :

- PGPID se veut neutre quant à son utilisation. Il est tout à fait possible de créer des services a-centrés comme des totalement centrés autour d'une seule entité. Dans les deux cas, l'utilisateur à un identifiant unique. Son réseau personnel et ~~ses~~ son identifiant restent identique, quel que soit son hébergeur du moment.
- Pour fonctionner, un système de ce genre n'a pas besoin d'être très étendu et utilisé. Un bout de serveur toujours allumé dans un coin pour faire office de TN de référence. PGPID peut même être utilisé «en solo» par un seul utilisateur qui s'en servirait pour identifier ses machines.
- Pour fonctionner à grande échelle (comprendre madame Michu), il faut que les outils soient le plus simple possible.
Il faut donc imaginer que :
 - Le TN de référence soit connu des outils, s'inscrire doit revenir à appuyer sur un bouton et rentrer son mot de passe.
 - Il soit possible de faire des recherches de personnes et ainsi retrouver ses contacts.
 - On utilise des QRCode et autres NFC. Il me suffit de scanner le QRcode d'un ami avec mon téléphone intelligent pour l'ajouter.
 
Quoi qu'il en soit, il faut voir ce qui est décrit plus au comme un draft. J'insiste sur le fait que c'est très loin d'être une spec (vous vous en serez rendu compte de vous-même). Beaucoup de choses peuvent changer et c'est en partie pour ça que j'en parle. Qu'est-ce que vous en pensez ? C'est l'idée ou la fausse bonne idée du siècle ? Je mérite le Turing Award ou la fosse commune ? Ou un peu entre les deux, voir les deux ?

# Conclusion bis (aka vaporware) #

Alors, c'est cool, vous voulez vous y mettre. Et vous vous demandez : "Quand est-ce que ça sort ?"

Et bien je vous répondrai : À moins que ma boite veuille copier google et m'offrir 20 % du temps pour des projets perso, ça mettra longtemps avant d'arriver.
J'ai déjà d'autres projets perso en cours et, hélas, mon temps libre a des limites. J'ai bien écrit deux bouts de code en python qui s'échangent des clés PGP mais c'est loin d'être utilisable et je ne sais absolument pas quand ça le sera...


Ça y est, j'ai terminé. C'est à vous d'écrire maintenant : à vos commentaires !!
