menu-position: 1
title:   Sortie de Fedora 22
id: fedora_22
css-template: article.css
color1: #db3279
index-style: pure-u-1 pure-u-md-2-3
authors: M5oul
         Stéphane Aulery, eggman, BAud, palm123, Renault, Sébastien Wilmet, Nils Ratusznik, CheckMatt, Benoît Sibaud, Julien.D, Jiehong et Atem18
date:    2015-04-21T20:17:12+02:00
license: CC by-sa
tags:    red_hat, distrib, distribution, fedora, sortie_version et sortie
---


Ce mardi 26 mai 2015, le projet Fedora a [annoncé](https://fedoraproject.org/wiki/F22_release_announcement) la sortie de la distribution GNU/Linux Fedora 22. La version 21 était sortie le 9 décembre 2014.



Pour rappel, Fedora est une distribution GNU/Linux communautaire développée par le projet éponyme et sponsorisée par l’entreprise Red Hat, qui lui fournit des développeurs ainsi que des moyens financiers et logistiques. Fedora est prompte à inclure des nouveautés et peut être considérée comme une vitrine technologique pour le monde du logiciel libre, auquel elle contribue largement via les projets amont tels que le noyau Linux, GNOME, NetworkManager, PackageKit, PulseAudio, X.Org, la célèbre suite de compilateurs GCC et bien d’autres.

----

[L’annonce de publication](http://article.gmane.org/gmane.linux.redhat.fedora.core.announce/2990)
[Les notes de version en anglais](https://docs.fedoraproject.org/en-US/Fedora/22/html/Release_Notes/)
[Site officiel du projet Fedora](https://getfedora.org/fr/)
[Site de la communauté francophone de Fedora](http://www.fedora-fr.org/)
[Précédente dépêche LinuxFr.org : sortie de Fedora 21](https://linuxfr.org/news/sortie-de-fedora-21)
[Article Wikipédia de Fedora](https://fr.wikipedia.org/wiki/Fedora_%28Linux%29)
[Téléchargement direct (officiel)](https://getfedora.org/fr/)
[Téléchargement version ARM (officiel)](https://arm.fedoraproject.org/)
[Tutoriel FedUp : Passer à la version supérieure de Fedora](http://doc.fedora-fr.org/wiki/FedUp_:_Passer_%C3%A0_la_version_sup%C3%A9rieure_de_Fedora)
[Télécharger Fedora 22 en torrent (officiels)](http://torrent.fedoraproject.org/)

----

Ceux qui désirent une plus grande stabilité et un support à long terme (pendant 10 ans) peuvent se tourner vers [Red Hat Enterprise Linux] (http://www.redhat.com/en/technologies/linux-platforms/enterprise-linux) (RHEL), qui est un dérivé de Fedora, ou [CentOS](http://centos.org/) et [Scientific Linux](http://www.scientificlinux.org/), qui sont des clones gratuits de RHEL.



Mais pour tous les autres, sortez les tacos au poulet et la sauce piquante : Fedora 22 est là !


# Environnement graphique



## Environnements de bureau



### GNOME



L’environnement de bureau par défaut [[GNOME]] est proposé en [version 3.16](https://linuxfr.org/news/gnome-3-16-nettoyage-de-printemps). Des contributeurs de Fedora se sont directement impliqués dans la réalisation de deux de ses nouveautés majeures :


- le peaufinage du design de l’explorateur de fichier _[Nautilus](https://wiki.gnome.org/Apps/Nautilus/)_, qui se coule mieux dans le visuel des autres applications GNOME ;
- la refonte globale du système de notification, abandonnant la présentation sur la barre du bas pour fusionner avec le widget calendrier dans la barre du haut.


[![Gnome-Shell par défaut](http://pix.toile-libre.org/upload/original/1432739121.png)](http://pix.toile-libre.org/upload/original/1432737761.png)


On peut noter l’apparition de deux jeux de puzzle, un clone du [2048](https://fr.wikipedia.org/wiki/2048_%28jeu_vid%C3%A9o%29) et un [[taquin]].



Un travail important d’intégration des applications Qt a été mené, avec la fin de l’écriture d’_Adwaita_, le thème par défaut des applications [[GTK+]], ainsi qu’une meilleure gestion des notifications. Cette version est également l’occasion pour le thème de _GNOME-Shell_ d’arborer un style plus sobre.



Notons encore qu’_ABRT_, l’outil de rapports de bogues, utilise désormais les paramètres de confidentialité de GNOME pour n’envoyer que les informations souhaitées par le rapporteur. Il s’intègre aussi mieux au nouveau système de notification. En marge de cette version de GNOME, la notification automatique de fin d’exécution des tâches lancées dans _GNOME Terminal_ a été ajoutée.



Enfin, dans l’idée d'une meilleure intégration, _GNOME Logiciel_ remplace Packagekit pour installer automatiquement les codecs manquants à l’ouverture d’un fichier.


### Mate



Pour les nostalgiques de l’ère GNOME 2, [[MATE]] passe en [version 1.8](http://mate-desktop.org/blog/2014-03-04-mate-1-8-released/). Cette version s’intègre mieux avec le système, en utilisant upower pour la gestion d’énergie, PulseAudio pour s'occuper du son et systemd-logind pour ce qui est des sessions.

Le gestionnaire de fenêtre Marco se rend plus flexible avec la prise en charge d’un léger _tiling_ et _snapping_.



L’aide utilisateur a été remaniée et quelques effets graphiques supplémentaires pointent le bout de leur nez, notamment pour l’extinction de l’appareil.



Attention, beaucoup de noms de paquet sous la forme _mate-function_ ont changés en faveur du nom du programme. Par exemple, _mate-file-manager_ devient _caja_.


### KDE



[Plasma 5.3](https://linuxfr.org/news/plasma-5-3-si-tu-veux-m-essayer-c-est-pas-un-probleme) est maintenant la version par défaut de l’interface de [[KDE]]. Cette version apporte un nouveau thème plus clair et plus lisible nommé *Breeze*.


Grâce aux changements apportés par [[Qt]] 5, la bibliothèque graphique ayant servi à sa conception, l’interface est maintenant totalement gérée par l’accélération graphique matérielle via [[OpenGL]].


La compatibilité avec Wayland a été accrue, tout comme la gestion de l’énergie et du Bluetooth.


[![Plasma 5.3](http://blog.fedora-fr.org/public/renault/.KDE_bureau_m.png?2589931905546628114)](http://blog.fedora-fr.org/public/renault/KDE_bureau.png)


### Xfce



[[Xfce]] est mis à jour en [version 4.12](https://linuxfr.org/news/xfce-4-12-est-la) : à la clé, la prise en compte des écrans de très haute résolution, une meilleure gestion des configurations multi-écran et de la bibliothèque _libinput_ décrite plus bas.

Côté applicatif, les onglets dans le navigateur de fichiers _Thunar_ font leur apparition et l’éditeur de texte _mousepad_ est plus rapide et plus simple.



Pour finir, la transition vers [[GTK+]] 3 se poursuit avec le port des applets et du lecteur multimédia *parole*.


[![Xfce](http://blog.fedora-fr.org/public/renault/.Xfce_bureau_m.png?1488492992231443074)](http://blog.fedora-fr.org/public/renault/Xfce_bureau.png)


### LXDE : LXQt



[[LXDE]] est disponible depuis plusieurs années dans Fedora, l’objectif étant de fournir une alternative très simple et très légère tout en essayant d’être visuellement agréable. Cette fois, c’est son pendant Qt (plutôt que GTK+) qui est disponible, avec le même objectif, mais un visuel et une intégration orientée Qt : [[LXQt]] — anciennement Razor-Qt.


### Qtile



Un nouveau gestionnaire de fenêtres pavant fait son apparition, de son petit nom _qtile_. Il est entièrement écrit et configurable en Python. Comme beaucoup de ses semblables, tel _i3_, il est hautement scriptable. 


## Localisation



Depuis la version 19, le projet Fedora a entrepris de grands travaux pour améliorer la gestion des langues. Cette version ne fait pas exception avec l’ajout de polices de caractères adaptées à l’[[Odia]] et au [[Telugu]], deux langues indiennes.



Toujours au sujet des polices de caractères, _eurlatgr_ remplace _latarcyrheb-sun16_ comme police de caractères des consoles pour les langues écrites dans un alphabet latin ou grec. Pour les alphabets arabe, cyrillique et hébreu, _latarcyrheb-sun16_ reste en vigueur. Ces changements devenaient nécessaires, en raison de l’usage croissant d’Unicode.


Dans la continuité de ce travail, la langue indienne [[Marathi]] dispose d’une nouvelle méthode de saisie _Minglish_, adaptée à la population également anglophone. Cette méthode repose sur la traduction phonétique des caractères à partir de la prononciation alphabétique anglaise. Si une correspondance est trouvée, le mot est automatiquement transcrit dans l’alphabet de cette langue. Ainsi la saisie est plus rapide, notamment pour les mots qui nécessitent de parcourir des tables ou d'utiliser des combinaisons complexes à partir de `Alt` + `Gr`.


Le gestionnaire de paquet a aussi été modifié pour installer automatiquement les traductions des logiciels selon la langue système choisie.



## Wayland et libinput



La migration vers [Wayland](http://wayland.freedesktop.org/) se concrétise. Avec la précédente version de Fedora, il était déjà possible de tester Wayland en changeant le type de session dans [GDM](https://wiki.gnome.org/Projects/GDM) (quand on choisit son utilisateur et qu’on rentre son mot de passe).



Avec Fedora Workstation 22, la migration vers Wayland continue de manière progressive. Ainsi, GDM tourne dorénavant par défaut sur Wayland. Cependant, s’il n’arrive pas à se lancer avec Wayland, GDM est automatiquement redémarré avec le serveur X. La session GNOME, par contre, reste sous X.


Bien que GDM soit plus facile à gérer que le bureau GNOME, on peut supposer avec optimisme que Wayland gérera par défaut la session GNOME dans la prochaine version de Fedora.



Un autre changement majeur sous le capot est le passage à [libinput](http://www.freedesktop.org/wiki/Software/libinput/), tant pour Wayland que pour le serveur X. En effet, X.Org ne s’occupe pas seulement de l’affichage, mais aussi des périphériques d’entrée (_input_) comme le clavier, la souris, le pavé et l’écran tactile.



Le serveur X dispose de plusieurs pilotes d’entrée notamment _evdev_ et _synaptics_. _libinput_ a initialement été développé pour Wayland, mais par la suite un pilote équivalent a été développé pour X.org, sous le doux nom de _xorg-x11-drv-libinput_. L’architecture de libinput est plus solide ; sa gestion du _multi-touch_ est meilleure que celle de _synaptics_, qui n’était pas vraiment prévue pour cela.


L’interaction entre plusieurs périphériques d’entrée est aussi facilitée, par exemple la désactivation du _pavé tactile_ lors de la frappe au clavier (_cf._ Hans de Goede, _[Replacing Xorg input-drivers with libinput](https://www.youtube.com/watch?v=uIEoFKhzxI8)_, [DevConf](http://devconf.cz/), 7 février 2015, Brno).


## Mozilla Firefox avec GTK+ 3

Fedora est la première distribution à intégrer [Mozilla Firefox basée sur la bibliothèque d’interface graphique GTK+ 3](http://linuxfr.org/users/tankey/journaux/firefox-en-gtk3). Avec cette mise à niveau de [[GTK+]] 2 à GTK+ 3, les ralentissements rencontrés lors du défilement rapide d’une page web, via le pavé numérique, semblent avoir été éliminés. Le thème de GTK+ 3, fait que l’on se retrouve avec les écritures blanc sur un fond noir. Vous trouverez plus d’information dans ce [lien](https://copr.fedoraproject.org/coprs/dgoerger/firefox-gtk3/).


# Administration système


## Sous le capot



Au menu, un noyau [Linux 4.0](https://linuxfr.org/news/sortie-du-noyau-linux-4-0) encore plus performant. Il facilite notamment la vie des sysadmin et des développeurs avec une notation zen qui désencombre la mémoire de futilités ! Merci Oncle Linus, merci !


Systemd n’est pas en reste avec sa [219^e récidive](http://lists.freedesktop.org/archives/systemd-devel/2015-February/028447.html).


## DNF remplace Yum



La nouveauté la plus marquante de cette version est probablement le remplacement du [[gestionnaire de paquets]] par défaut. [DNF](https://en.wikipedia.org/wiki/DNF_%28software%29) (_Dandified Yum_) prend la place du vétéran [Yum](https://fr.wikipedia.org/wiki/Yellowdog_Updater,_Modified), présent depuis Fedora Core 1 (septembre 2003).


Plusieurs éléments ont mené à la création de DNF . Yum fait son âge et son évolution a laissé un code peu maintenable. Son cœur bat encore en Python 2 et son API est assez mal documentée. De surcroît son empreinte mémoire est importante et ses performances laissent à désirer. Partant de ce constat, Yum a été forké en janvier 2012, donnant naissance à DNF.


Les développeurs de DNF ont ainsi procédé à une réécriture et un nettoyage du code, abandonnant au passage certaines fonctionnalités et rendant l’outil compatible avec Python 3. Ils ont de même documenté l’API depuis le début du projet.


La gestion des dépôts a été délégée à _librepo_ et celle  des dépendances à _hawkey_, qui fournit une API de haut-niveau à [_libsolv_](https://github.com/openSUSE/libsolv). Cette bibliothèque a été développée à l’origine par [[openSUSE]] pour son gestionnaire de paquets [zypper](https://fr.opensuse.org/Portal:Zypper). Elle repose sur un algorithme de résolution de dépendances par [satisfaisabilité](https://fr.wikipedia.org/wiki/Probl%C3%A8me_SAT) bien plus efficace, rapide et économe en mémoire que l'algorithme itératif de Yum.


Présent dans Fedora depuis la [version 18](https://fedoraproject.org/wiki/Features/DNF#Current_status), DNF est maintenant considéré comme stable et remplace donc Yum comme gestionnaire de paquets par défaut. Il peut cependant être désinstallé au profit de Yum ou être utilisé en parallèle à celui-ci.



DNF n’est toutefois pas totalement compatible avec son prédécesseur. La liste des différences est documentée sur une [page ad-hoc](https://dnf.readthedocs.org/en/latest/cli_vs_yum.html). Afin de faciliter la transition, une couche de compatibilité _dnf-yum_ est également proposée, sans pour autant gommer les différences entre les deux gestionnaires de paquets.


Pour plus de renseignements, vous pouvez consulter la [FAQ](https://dnf.readthedocs.org/en/latest/user_faq.html) de DNF ainsi que la [documentation officielle](https://dnf.readthedocs.org/en/latest/index.html).

## Rechercher un logiciel dans un dépôt désactivé



Autre nouveauté sur la gestion des paquets : afin de simplifier la vie de l’utilisateur tout en l’incitant à désactiver les dépôts non officiels pour sa sécurité, il est dorénavant possible via PackageKit et GNOME Logiciels de rechercher des paquets disponibles dans des dépôts désactivés. L’information sur un dépôt désactivé est remontée à l’utilisateur seulement si l’option *enabled_metadata* est activée pour celui-ci.


## PreUpgrade facilite la mise à jour



Un nouveau paquet _preupgrade-assistant_ a été ajouté. Il permet d’aider l’administrateur système à adapter la configuration de ses logiciels à celle d’une version plus récente, si nécessaire.



## Dbxtool



La norme UEFI avec Secure Boot permet non seulement l’établissement d’une liste blanche de binaires et de matériel utilisables, mais également l’établissement d’une liste noire de binaires et de certificats de sécurité. Ça peut être une manière de forcer la mise à jour d’un logiciel non fiable par exemple.



Le nouvel utilitaire _dbxtool_ a donc été mis à disposition pour gérer la liste noire de l’UEFI Secure Boot. Il n’est toutefois pas actif par défaut.


## BIND



[[BIND]] fait une entrée remarquée avec la [version majeur 9.10](https://kb.isc.org/article/AA-01153/0/BIND-9.10.0-Release-Notes.html). Aussi, il est vivement recommandé aux administrateurs systèmes l’employant de se renseigner en détail à son sujet.



Pour résumer, l’ajout du type de fichier *map* permet à des données de zones d’être directement accessibles en mémoire. Un nouvel outil _delv_ aide dorénavant à l’inspection des données [[DNS]], mais aussi [[DNSSEC]]. L’API PKCS#11 est maintenant également disponible. Outre l’amélioration des performances globales du programme, elle permet l’exploitation du matériel d’accélération de calculs cryptographiques et  de bien d’autres encore.


## Apache



Dans les logiciels prisés par les administrateurs système, il y a aussi [Apache](https://fr.wikipedia.org/wiki/Apache_HTTP_Server). Ainsi, Fedora 22 inclut le programme [Ipsilon](https://fedorahosted.org/ipsilon/). Il s’agit d’un serveur et d’une boîte à outils destinés à configurer les fournisseurs de services basés sur Apache. Il inclut une application `mod_wsgi` qui fournit les SSO fédérés aux applications web. 

Un gestionnaire d’identité est fourni à côté pour l’identification via [FreeIPA](https://en.wikipedia.org/wiki/FreeIPA) par exemple, tandis que la communication avec les applications peut se faire via des protocoles fédérés, tels que [[SAML]] ou [[OpenID]].


## Serveur de base de données



L’aspect base de données n’est pas laissé de côté pour autant. Fedora fournit une suite d’utilitaires pour améliorer et simplifier le rôle de serveur de base de données. Le tout repose sur le gestionnaire de base de données libre [[PostgreSQL]].



Avec l’aide du programme graphique Cockpit et de l’interface [[D-Bus]], il devient plus simple et plus rapide de créer un système orienté base de données disponible sur le réseau. Pour la mise en place, elle repose sur le travail de la version précédente, à savoir l’API et le framework des _rôles de serveurs_.


## Système de fichier par défaut



Uniquement pour la version serveur de Fedora, le système de fichier [[XFS]] est proposé par défaut au lieu de [[ext4]] pour sa capacité à gérer de manière optimale les grands espaces de stockage (8 Eio au lieu de 1 Eio, et 8 Eio au lieu de 16 Tio pour la taille maximale d’un fichier).

# Cloud et virtualisation



## Elasticsearch



L’outil [Elasticsearch](https://www.elastic.co/products/elasticsearch) fait son apparition dans les dépôts. Ce logiciel est un serveur distribué d’indexation de données. Basé sur une API [[REST]] et orienté documents, il stocke ses résultats sous forme de documents [[JSON]]. L’objectif est de faciliter les recherches dans un grand nombre de données textuelles et potentiellement non formatées, mais il peut également indexer en fonction d’un schéma JSON ou XML par exemple.


## Fedora Atomic Host



[Fedora Atomic Host](https://fedoraproject.org/wiki/Changes/AtomicHost), l’implémentation du projet [Atomic](http://www.projectatomic.io) au sein de Fedora, est disponible. L’objectif du projet Atomic est de fournir de manière sécurisée des applications à base de Docker en couplage avec SELinux et des mises à jour dites atomiques.


Ce comportement a nécessité la conception d’un nouvel outil, _RpmOstree_, reposant sur les bibliothèques de DNF évoquées plus haut. Il représente les mises à jour sous forme d’un arbre de modifications à appliquer de façon atomique, plutôt que paquet par paquet. Il peut permettre typiquement la création d’une partition clone de celle d’origine avec les mises à jour effectuées. Ainsi, en cas de panne, il peut revenir en arrière très rapidement et simplement, sans casse.


## Tunir



Dans la même optique de déploiement et de test d’images _cloud_, on notera la disponibilité de _[Tunir](http://tunir.readthedocs.org/en/latest/)_. Ce logiciel d’aide à l’intégration continue est utilisé notamment pour les images officielles de Fedora Cloud compilées chaque nuit. Il peut ainsi construire une image pour la lancer automatiquement dans une machine virtuelle ou dans Docker pour y exécuter les tests adéquats. Il se distingue de solutions comme Jenkins par sa légèreté et sa simplicité d’utilisation.


## Vagrant



On remarquera l’ajout de _[Vagrant](https://www.vagrantup.com/)_, un utilitaire d’automatisation et de gestion de création de machines virtuelles pour faciliter le développement et la réalisation de tests dans un environnement unique.



Bien qu’il puisse être couplé avec plusieurs systèmes de virtualisation comme VirtualBox, Docker et VMWare, il utilise par défaut la solution _libvirt_, largement répandue dans l’écosystème Fedora & Red Hat. Par ailleurs, Vagrant peut exploiter les images Fedora Atomic Host et Fedora Cloud pour générer des systèmes prêts à l’emploi.

# Développement



## GCC 5.1



La suite de compilation GCC est mise à jour vers la nouvelle version majeure 5.1 (_cf._ la dépêche [Le compilateur GCC 5.1 : harder, better, faster, stronger](//linuxfr.org/news/le-compilateur-gcc-5-1-harder-better-faster-stronger#nouveaut%C3%A9s-concernant-les-langages)). L’[ABI](http://fr.wikipedia.org/wiki/Application_binary_interface) des applications C++ change pour se conformer intégralement à la norme C++11, qui nécessite la recompilation de tous les programmes utilisant `std ::list` et `std ::string`. Cependant, pour des raisons de temps, Fedora a compilé l’ensemble des programmes avec GCC 5, mais en désactivant la nouvelle ABI, ce qui retarde la recompilation massive de ces programmes à Fedora 23.

## Unicode 7.0



La bibliothèque standard du langage C, glibc, prend en charge [Unicode 7.0](http://www.unicode.org/versions/Unicode7.0.0/) au lieu d’Unicode 5.1. L’ajout de ces 8 000 caractères supplémentaires a nécessité un gros travail pour corriger les régressions potentielles dues à ce changement. Cela permet de poursuivre l’effort d’internationalisation du projet.


## Boost



Les amateurs de C++ seront heureux d’apprendre le lifting de la bibliothèque [Boost](https://fr.wikipedia.org/wiki/Boost_%28biblioth%C3%A8ques%29) en [version 1.57](http://www.boost.org/users/history/version_1_57_0.html). Ce petit boost corrige de nombreux bogues, surtout dans la section _Thread_, en attendant l’arrivée prochaine de la [version 1.58](http://www.boost.org/users/history/version_1_58_0.html) qui incorporera notamment une classe pour la gestion du [boutisme](http://fr.wikipedia.org/wiki/Endianness), mais aussi une classe dédiée aux tris de haute performance.


## Python



Le framework web _[Django](https://fr.wikipedia.org/wiki/Django (framework))_ passe à la [version 1.8](https://docs.djangoproject.com/en/dev/releases/1.8/). L’API _Model.meta_ apparaît. Plusieurs moteurs de _templates_ comme celui de Django ou Jinja2 sont gérés. Une meilleure intégration à PostgreSQL pour l’aspect base de données et divers correctifs améliorent encore l’environnement [Python](https://fr.wikipedia.org/wiki/Python_%28langage%29).


La bibliothèque graphique _[[wxPython]]_ passe à la [version 3.0](http://wxpython.org/recentchanges.php) (la version précédente datait de 2011). Elle gère maintenant GTK+ 3, apporte le rendu des pages HTML/CSS/JS via wx.html2 et corrige énormément de bogues. En théorie, la compatibilité avec la version précédente n’est pas un grand problème, mais doit être vérifiée par ses utilisateurs.


La bibliothèque _[dateutil](https://dateutil.readthedocs.org/en/latest/)_ se met à l’[heure 2.4](https://github.com/dateutil/dateutil/blob/master/NEWS). Par rapport à la version 1.5, le gros changement est la compatibilité avec Python 3. La version compatible avec Python 2.6 est disponible dans un autre paquet. Cette bibliothèque étend la gestion des dates standards avec le calcul relatif des dates (la dernière semaine d’un mois donné par exemple), le calcul de la date de Pâques, une meilleure gestion des fuseaux horaires via _tzdata_.


## Ruby



Le précieux [[Ruby]] s’orne d’une nouvelle [version 2.2](https://github.com/ruby/ruby/blob/v2_2_0/NEWS). Ce bijou apporte le fameux [ramasse-miettes](http://fr.wikipedia.org/wiki/Ramasse-miettes_%28informatique%29) incrémental et s’occupe également des symboles. La prise en charge d’Unicode se confirme avec la version 7.0. Quelques méthodes supplémentaires ont été ajoutées sur les types de base. Un nettoyage de l’API C a également été mené, pour terminer le polissage.


Ruby met son framework web sur les rails avec la [version 4.2](http://guides.rubyonrails.org/4_2_release_notes.html). Le train des nouveautés comporte _l’ActiveJob_, une couche d’abstraction pour les systèmes d’attente tels que _Resque_, _Delayed Job_ ou _Sidekiq_. L’_ActionMailer_ peut envoyer les messages après un délai d’attente, tandis que la sérialisation des données peut se faire avec _GlobalID_. La console web n’est pas oubliée sur le quai étant donné qu’elle facilite le développement et le débogage d’applications.


## Perl



Le langage de programmation [Perl](https://fr.wikipedia.org/wiki/Perl_%28langage%29) passe en [version 5.20.0](http://perlnews.org/2014/05/perl-5-20-released/) (cf. [perldelta 5.20.0](http://search.cpan.org/dist/perl-5.20.0/pod/perldelta.pod)). Décidément, c’est la fête d’Unicode, mais cette fois c’est pour la [6.3](http://unicode.org/versions/Unicode6.3.0/) et, si l’UTF-8 est la locale de l’environnement, le traitement des caractères passe en Unicode.



Deux nouvelles syntaxes se révèlent être de vraies perles : *%hash{...}* et *%array{...}* pour obtenir des listes de clés/valeurs ou d’index/valeurs respectivement. Le Copy-on-write est accessible pour les données scalaires. À côté de ça, nous pouvons noter la disparition de *PerlIO_vsprintf()* et de *PerlIO_sprintf()*. 


## Haskell



Le compilateur du langage fonctionnel [[Haskell]], surnommé [GHC](https://fr.wikipedia.org/wiki/Glasgow_Haskell_Compiler), fonctionnera sous la [version 7.8](https://downloads.haskell.org/~ghc/7.8.1/docs/html/users_guide/release-7-8-1.html). Au menu, un nouveau gestionnaire d’entrées-sorties améliore les performances en cas d’activité intense dans ce domaine. Le nouveau générateur de code est enfin accessible après des années d’attente, promettant un gain de performance perceptible ; l’ancien n’est de toute façon plus disponible. Avec l’aide de la suite de compilation [[LLVM]], la compilation croisée à destination des systèmes [iOS](https://fr.wikipedia.org/wiki/IOS_%28Apple%29) est désormais possible. Enfin, la compilation parallèle est aussi disponible de manière similaire à ce que propose la commande *make*.


## Java



[[Gradle]], le constructeur, déployeur et testeur automatique d’applications [Java](https://fr.wikipedia.org/wiki/Java_%28langage%29), bénéficie aussi d’un rafraîchissement à la [mode 2.0/2.2](http://gradle.org/docs/2.0/release-notes.html). C’est ainsi que la compatibilité avec Java 8 est proposée, tandis que la prise en charge de Java 5 est entièrement retirée. La compilation croisée en est facilitée, tout comme la gestion de l’architecture 64 bits de [[Windows]]. Sachez enfin que les utilisateurs d’[OpenShift](https://en.wikipedia.org/wiki/OpenShift) profiteront aussi de sa compatibilité avec son infrastructure.


# Architecture ARM



Précédemment, les images pour l’[[architecture ARM]] étaient sur la page des [Spins](https://spins.fedoraproject.org/). À présent, une [page](https://arm.fedoraproject.org/) leur est dédiée, avec des images pour les versions _Serveur_ et _Workstation_ qui intègrent la majorité des environnements de bureau.



Vous trouverez les informations spécifiques à certaines cartes ARM sur le [wiki](https://fedoraproject.org/wiki/Architectures/ARM) Fedora.


# Prévus pour la suite



## 23 dans les tuyaux



La [version 23](https://fedoraproject.org/wiki/Releases/23/Schedule) a déjà quelques pistes de nouveautés qui devraient voir le jour. Comme cela a déjà été expliqué plus haut, Wayland devrait remplacer totalement X11, laissant XWayland pour la compatibilité, si nécessaire.


Après des années de travaux en ce sens, [Python 3 pourrait également être à l’honneur comme la seule version installée](https://fedoraproject.org/wiki/Changes/Python_3_as_Default). Python 2.7 serait accessible dans les dépôts pour ceux qui le souhaitent.


De nouveaux rôles de serveur, en plus de celui de serveur de base de données, devraient être ajoutés pour simplifier l’administration système.


## Le futur de Fedora Workstation



Red Hat [a rejoint](https://blogs.gnome.org/uraeus/2015/04/17/red-hat-joins-khronos/) récemment le groupe [Khronos](https://www.khronos.org/), qui s’occupe notamment de la standardisation d’_OpenGL_. Red Hat aura donc son mot à dire. Gageons que ce sera un gain pour le desktop sous GNU/Linux.



Il est aussi prévu dans _GNOME Logiciels_ de pouvoir mettre à jour le _firmware_ des cartes mères ou d’autres périphériques, chose qui est généralement faisable uniquement sous Windows pour le moment.



Enfin, Fedora vise une consommation moindre d’énergie pour une plus grande durée de vie des batteries (_cf._ Christian Schaller, [Fedora Workstation: More than the sum of its parts](https://blogs.gnome.org/uraeus/2015/04/20/fedora-workstation-more-than-the-sum-of-its-parts/), 20 avril 2015).
