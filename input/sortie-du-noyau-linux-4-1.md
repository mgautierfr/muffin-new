menu-position: 2
title:   Sortie du noyau Linux 4.1
id: linux_41
css-template: article.css
color1: #e59728
index-style: pure-u-1 pure-u-md-1-3
authors: Martin Peres
         eggman, Davy Defaud, Siosm, BAud, palm123, M5oul, alpha_one_x86, Stéphane Aulery, Mali, tiwaz, Romain Perier, xaccrocheur, Olivier Esver, Xavier Claude, Benoît Sibaud, jcr83, pums974, ʭ  ☯ , Nÿco, mimaison, Ytterbium, Segfault, Kioob, Vincent Meurisse, titinux, EdB et Jiehong
date:    2015-04-25T00:19:43+02:00
license: CC by-sa
tags:    kernel
---


La sortie de la version stable 4.1 LTS du noyau Linux a été annoncée le 21 juin 2015 par Linus Torvalds. Le nouveau noyau est, comme d’habitude, téléchargeable sur les serveurs du site [_kernel.org_](http://www.kernel.org). Le détail des évolutions, nouveautés et prévisions se trouve dans la seconde partie de la dépêche.

----

[Les noyaux précédents](http://linuxfr.org/wiki/depeches_noyau)
[Site officiel du noyau Linux](http://www.kernel.org/)

----

# En bref


# Annonces des versions candidates par Linus Torvalds

## RC-1
La version [RC-1](https://lkml.org/lkml/2015/4/26/269) est sortie le dimanche 26 avril 2015 :

> Cette fenêtre d’intégration a été normale et je publie, conformément au calendrier prévu. Mes quelques jours de voyage n’ont pas interféré, car j’avais toujours accès à Internet.
> 
> La fenêtre d’intégration est assez normale aussi, par rapport à ce qui a été fusionné.
En ne regardardant que la taille, on se dit que ça va tout juste rentrer — alors que la 4.0 était un peu plus petite que d’habitude — la 4.1 semble pile dans la fourchette normale des deux dernières années. Et les statistiques des correctifs aussi ont l’air normales : le gros des changements concerne les pilotes (à peine moins de 60 % des correctifs), avec des mises à jour d’architecture autour de 20 % et le reste est disséminé un peu partout.
>
> Aucune nouvelle fonctionnalité fracassante ne me vient à l’esprit, même si la prise en charge initiale de l’ACPI pour ARM 64 bits semble amusante. Selon vos intérêts, votre appréciation de « grande nouveauté » peut évidemment différer de la mienne. Il y a beaucoup de travail partout et certains points pourraient bien faire une grande différence selon vos usages.
>
> Donc, allez‐y, testez. Même une RC-1, aussi brute qu’elle puisse parfois être, a tendance à être de bonne qualité. Ce n’est pas si effrayant. Promis.
>
> Linus

## RC-2
La version [RC-2](http://lkml.iu.edu/hypermail/linux/kernel/1505.0/01622.html) est sortie le dimanche 3 mai 2015 :

> Ces derniers temps, les RC-2 ont été plutôt petites — ressemblant plus à des RC tardives. Il _fut un temps_ où je ne pouvais même pas poster le journal abrégé, parce qu’il était trop long. Ça n’a pas été le cas pour les quelques dernières versions.
>
> Je pense que les gens ont tendance à prendre un moment de repos après la fenêtre d’intégration, parce que les RC-3 ont tendance à être un peu plus grosses à nouveau. Mais c’est peut‐être aussi parce que je suis devenu meilleur à dire « la fenêtre d’intégration est terminée, je n’accepte pas les traînards » ou que les gens respectent mieux la fenêtre d’intégration. Quelle qu’en soit la raison, le temps des énormes RC-2 semble être heureusement révolu.
>
> Donc, le cycle de développement de 4.1 correspond à ce nouveau modèle. Même si elle n’est pas _aussi_ petite que, disons, la 3.19-rc2 ne l’était (elle était vraiment exceptionnelle), la 4.10-rc2 [_sic_] est plutôt petite et tout a été assez calme. Le _diffstat_ est plutôt plat et propre, à l’exception notable du nouveau générateur de nombres pseudo‐aléatoires basé sur SHA512 pour s390. Je suppose que j’aurais dû prendre aussi tout ça en main. Mais le reste a vraiment l’air de petits correctifs, pas de gros nouveaux morceaux de code.
>
> Comme d’habitude, c’est un mélange de correctifs de pilotes, de mises à jour d’architectures (avec s390 se démarquant à cause d’un changement sur le générateur de nombres pseudo‐aléatoires) et d’un peu de systèmes de fichiers et de réseau. Le journal abrégé ci‐joint contient les détails, il n’y a rien de particulièrement inquiétant. Jusqu’à présent, la 4.1 a l’air plutôt normale.
>
> Linus

## RC-3
La version [RC-3](https://lkml.org/lkml/2015/5/10/164) est sortie le dimanche 10 mai 2015 :

> Et une version de dimanche de fête des mères pour vous, que vous soyez maman ou non. Car, eh, c’est dimanche après‐midi une fois encore et c’est ainsi ça que sortent mes versions candidates.
>
> Rien de particulièrement effrayant ou inquiétant n’est à signaler, bien qu’il y ait de petites réparations un peu partout. Certaines sont les régressions de cette fenêtre d’intégration et d’autres sont plus vieilles. Et certaines sont si vieilles que j’ai presque pensé que « si c’est cassé depuis 2011 et que tu le remarques seulement maintenant, ça aurait peut‐être pu attendre la prochaine fenêtre d’intégration .» Vous vous reconnaîtrez (et les autres aussi, s’ils lisent les messages de _commit_ — votre honte s’y retrouve).
>
> Le journal abrégé en annexe donne un aperçu honnête et il n’est pas trop gros. Comme vous pouvez le voir, c’est surtout des pilotes, avec une poignée de mises à jour d’architectures (surtout ARM). Et une poignée de trucs divers : outils de performances, documentation, systèmes de fichiers. La mise à jour d’InfiniBand est un part important des pilotes, on a eu des trucs retardés à cause de changements dans la maintenance.
>
> Allez‐y, testez. À la RC3, les choses devraient être plutôt sans danger et ce serait le moment de vérifier que tout fonctionne correctement, si vous n’avez pas déjà testé un des noyaux de développement précédents.
>
> Linus

## RC-4
La version [RC-4](https://lkml.org/lkml/2015/5/18/595) est sortie le lundi 18 mai 2015 :

> Celle‐ci a raté ma publication dominicale habituelle, parce que j’étais sorti presque toute la journée d’hier. De plus, il y avait une régression de la mise en veille qui semblait devoir recevoir un correctif imminent, donc ça ne me dérangeait pas de faire une publication tard dans la soirée, dans l’espoir d’une rustine supplémentaire.
>
> Ça y est, correctifs de dernière minute et tout. Le correctif RC-4 est un peu plus gros que les précédents, mais ça semble dû principalement à des aléas temporels normaux — simplement la fluctuation liée à l'arrivée de l’arborescence des sous‐mainteneurs — et le fait que la RC-4 contienne les correctifs de Greg dans diverses arborescences de pilotes _et_ les correctifs de Davem concernant le réseau, de sorte que la taille légèrement plus importante n’est pas représentative de soudains problèmes.
>
> Bien que les pilotes et le réseau dominent les changements, il y a une poignée de changements épars : mises à jour des systèmes de fichiers (Btrfs et NFS), mises à jour d’architectures (principalement ARM, ARM64 et MIPS) et d’autres changements divers.
>
> Allez‐y, testez et avertissez chacun de la moindre régression trouvée.
>
> Linus

## RC-5
La version [RC-5](https://lkml.org/lkml/2015/5/24/212) est sortie le dimanche 24 mai 2015 :

> Je suis de retour à mon habituel calendrier dominical et la RC-5 est de retour à sa taille habituelle, après une légère augmentation de la RC-4.
>
> Les choses semblent assez normales. Nous avons à peu près deux tiers de mises à jour de pilotes (pilotes graphiques, InfiniBand, audio, réseau, SCSI et température) et quasiment la moitié de ce qui reste est une mise à jour du réseau. Le reste est principalement des mises à jour d’architectures et quelques correctifs des systèmes de fichiers. Mais tout cela est plutôt restreint.
>
> Donc, nous sommes dans les temps pour une 4.1 normale, si ce n’est que le calendrier de la prochaine fenêtre d’intégration semble se télescoper avec nos vacances familiales annuelles. Donc nous allons voir comment ça va marcher. Je pourrais finir par retarder la publication juste pour éviter cela (ou simplement retarder l’ouverture de la fenêtre d’intégration). Je n’ai pas encore décidé.
>
> Mais, s’il vous plaît, continuez à tester.
>
> Linus

## RC-6
La version [RC-6](https://lkml.org/lkml/2015/5/31/227) est sortie le dimanche 31 mai 2015 :

> Ça a été une semaine plutôt normale, bien que je ne puisse pas dire que les versions candidates aient précisément déjà commencé à diminuer. Non, les RC n’ont pas été suffisamment grandes pour commencer un cycle de publication, et les choses ont été plutôt calmes, mais je serais plus heureux si l’on n’avait pas de bruit dans le RAID 5 et la carte des périphériques [_device‐mapper_] à cette étape.
>
> Cela dit, ce n’est pas comme si la RC-6 était une grosse version candidate, et les choses paraissent normales. Il s’agit pour la moitié environ de pilotes (principalement des cibles SCSI, du réseau et du graphique, plus les modifications du RAID et de la carte des périphériques mentionnées plus haut, avec d’autres corrections diverses).
>
> Le reste est assez également réparti entre les mises à jour d’architectures (Alpha sort du lot), des mises à jour de systèmes de fichiers (XFS, CIFS et OverlayFS) et « divers » (réseau, l’outil de mise à jour de `turbostat` et la documentation).
>
> La plupart des correctifs sont plutôt petits. Le journal abrégé est joint, il donne une bonne idée des choses que nous avons ici.
>
> Linus

## RC-7
La version [RC-7](https://lkml.org/lkml/2015/6/7/219) est sortie le dimanche 7  juin 2015 :

> Encore un dimanche, encore une RC.
>
> Normalement, la RC-7 tend à être la dernière version candidate, et il n’y a pas grand‐chose qui mérite notre attention ce coup‐ci. Cependant, nous avons encore quelques régressions et, comme je l’ai mentionné la semaine dernière, mes vacances familiales annuelles arrivent. Nous aurons donc une RC-8 et une semaine supplémentaire avant que la RC-8 ne soit réellement publiée.
>
> Ça s’est agréablement calmé cependant et la RC-7 vaut certainement la peine d’être testée pour vérifier que tout fonctionne pour vous. Parce qu’on est sacrément proche de la version finale et qu’elle devrait être assez sûre à tester. Les changements apparus la semaine dernière sont plutôt restreints. Le journal abrégé en annexe devrait vous donner un aperçu de ce qui se passe. Beaucoup d’entre eux sont des correctifs d’une ligne, avec quelques correctifs légèrement plus gros sur certaines architectures (des contraintes de performance sur x86, quelques mises à jour sur SPARC), ainsi que des pilotes en attente d’intégration [branche _staging_].
>
> Linus

## RC-8
La version [RC-8](https://lkml.org/lkml/2015/6/14/250) est sortie le dimanche 14 juin 2015 :

> Je suis en vacances, mais le temps ne s’arrête pas pour autant et c’est dimanche, donc le moment de sortir une RC que j’espère finale.
>
> Il se trouve que je voulais faire traîner la publication d’une semaine, de sorte à ne pas avoir la fenêtre d’intégration pendant mes vacances — nous avons encore quelques correctifs dans _md_. Comme Neil Brown l’a dit : « ça n’a pas été un bon cycle pour _md_ :-( ».
>
> Les correctifs sont assez petits et j’espère que tout va bien maintenant. Mais une semaine supplémentaire pour tester ne va certainement pas faire de mal, donc une RC-8 est tout à fait appropriée.
>
> Il y a aussi diverses choses qui avancent, dont les correctifs MIPS, ainsi que de petites mises à jour pour ARM, s390 et x86.
>
> Mais le gros est — comme d’habitude — les pilotes et, non, ça ne vient pas du camp _md_ — ces correctifs sont très petits. Il s’agit principalement d’Ethernet, du DMA esclave [_slave DMA_] et de _spund_, ainsi que des correctifs DRM et d’autres choses.
>
> Il y a aussi quelques correctifs de réseau et diverses petites choses. Le journal abrégé est joint comme d’habitude, pour les personnes qui veulent un aperçu des détails.
>
> Quoi qu’il en soit, c’est pas comme s’il y avait une *tonne* de correctifs. La plupart d’entre eux sont de petits correctifs, donc je ne pense pas que ce soit particulièrement inquiétant. C’est simplement que la RC-8 sort non seulement à cause de mon agenda, mais aussi à cause de petits détails qui surgissent sans cesse.
>
> Faisons en sorte que la semaine prochaine soit vraiment calme. Le pouvons‐nous ? Parce que je vais très activement éviter de lire mes courriels.
>
> Linus

## Version finale
La [version finale](https://lkml.org/lkml/2015/6/22/8) est sortie le dimanche 21 juin 2015 :

> Bon, après une semaine *très* tranquille passée la publication de la RC-8, la version 4.1 finale est maintenant publiée.
>
> Je ne suis pas sûr que ce fût calme parce qu’il n’y avait aucun problème — touchons du bois — ou parce que les gens ont décidé d’épargner mes vacances, mais, quelle qu’en soit la raison, j’apprécie. Ça n’est pas comme si le cycle de sortie de la 4.1 avait été particulièrement pénible, espérons que la semaine supplémentaire de décantation ait permis d’en faire une meilleure version. Ça n’est pas une mauvaise chose, puisque la 4.1 est une version à support à long terme [LTS].
>
> Quoi qu’il en soit, depuis la RC-8, nous avons eu des changements vraiment petits, principalement quelques corrections finales de pilotes (le son HDA, le DRM, les périphériques de stockage SCSI et la cryptographie) et quelques petits correctifs divers. Le journal abrégé ci‐joint est probablement le plus petit jamais écrit. Je ne m’en plains pas.
>
> Et cela signifie évidemment que la fenêtre de fusion de la 4.2 est maintenant ouverte.
>
> Linus

# Les nouveautés

## Architecture


### Gestion du matériel

#### Qualcomm MSM8916


Le Snapdragon 410 est la nouvelle famille de systèmes monopuce ARM 64 bits (ARM v8) de chez Qualcomm. Le MSM8916 intègre quatre cœurs ARM Cortex A53, gravés en 28 nm et cadencés à une fréquence de 1,4 GHz. Il adopte une architecture de type Harvard, en séparant les données et les instructions du cache de niveau 1. Il incorpore un processeur graphique Adreno 306 et un processeur de signal Hexagon QDSP6.

Il fait partie des nouveaux systèmes monopuces ARM64 à faire leur apparition dans Linux 4.1.

#### Xilinx ZynqMP


Xilinx est un constructeur américain principalement connu pour ses périphériques et puces à logique programmable. Également connu comme étant l’inventeur du [FPGA](https://fr.wikipedia.org/w/index.php?title=Circuit_logique_programmable), il se focalise aujourd’hui sur la conception de systèmes monopuces programmables, combinant des cœurs ARM à des FPGA. Cela vous donne la possibilité de rerouter électriquement l’intégralité d’une carte de développement ou de développer n’importe quel périphérique à l’intérieur du FPGA intégré à la puce (un périphérique Ethernet PHY qui gère du PTP, par exemple).

Parmi les deux puces les plus connues, nous pouvons citer le Zynq-7000 et le Zynq UltraScale MPSoC, aussi appelé ZynqMP. C’est à ce dernier que nous nous intéresserons.

Le ZynqMP est le premier système monopuce programmable 64 bits de chez Xilinx, il s’agit de la gamme de puces haute performance qui dispose d’un processeur à quatre cœurs ARM Cortex A53, des coprocesseurs Cortex-R5 dits temps réels, un processeur graphique ARM Mali 400MP, la gestion de la mémoire DDR4. Bref, vous l’aurez compris, c’est une véritable machine de guerre qui fait tout, sauf repasser votre linge. :/

Linux 4.1 intègre la gestion de ce nouveau système monopuce. Restent maintenant à venir les cartes de développement et les kits d’évaluation.

### L’ajout de la prise en charge de l’Intel Skylake continue

L’outil Turbostat est fourni avec le noyau Linux pour analyser statistiquement les performances des processeurs Intel récents. Il a été amélioré pour les [spécificités](http://www.phoronix.com/scan.php?page=news_item&px=Intel-Turbostat-Skylake) des processeurs Skylake.

### Intel Bay Trail & Cherry Trail
Ces processeurs obtiennent un [changement de réglage par défaut](http://www.phoronix.com/scan.php?page=news_item&px=Intel-Bay-Cherry-Faster-4.1), qui donnera de meilleures performances quand il faut de la puissance de calcul, tout en limitant la consommation supplémentaire. Celle‐ci peut toujours être maîtrisée avec le profil « _powersave_ ». Concrètement, le « _setpoint_ » par défaut est passé de 97 à 60.

### Meilleure gestion de l’énergie


Ce noyau gère mieux l’énergie pour les [x86](http://www.phoronix.com/scan.php?page=news_item&px=Linux-4.1-ACPI-PM) et les [ARM](http://www.phoronix.com/scan.php?page=news_item&px=Linux-4.1-ARM64-ACPI), avec une meilleure prise en compte de l’ACPI (ARM 64 bits inclus), une gestion du futur matériel Intel, la gestion des mises à jour pour les domaines de puissance : le noyau peut maintenant gérer plus finement les économies d’énergie, car il connaît la topologie de la dissipation de la puissance. Par exemple, si le processeur et le processeur graphique sont sur la même puce ([_die_](https://fr.wikipedia.org/wiki/Die_%28circuit_int%C3%A9gr%C3%A9%29)), il s’agit du même domaine de puissance.

L’architecture ARM64 prend désormais en charge l’_Advanced Configuration and Power Interface_, aussi appelée ACPI. La prise en charge de l’ACPI pour l’ARM a été controversée par le passé : de nombreux développeurs préféreraient utiliser de façon universelle le mécanisme d’arborescence de périphériques pour la détection des périphériques sur cette architecture. L’ajout de l’APCI s’est fait tranquillement cependant, et il semble très vraisemblable qu’il y ait des serveurs utilisant l’ACPI en vente dans un futur proche. Cela dit, il reste du travail : le _commit_ de fusion précise que « nous n’allons prendre en charge aucun périphérique pour le moment, donc son champ d’application est assez limité ». Voir `Documentation/arm64/arm-acpi.txt` pour de plus amples informations à propos de l’ACPI sur ARM.

Voir l’article de _LWN.net_ : [_ACPI for ARM?_](http://lwn.net/Articles/574439/), [[_correctif de la demande d’inclusion_](http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=836ee4874e201a5907f9658fb2bf3527dd952d30)], [`Documentation/arm64/arm-acpi.txt`](http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/Documentation/arm64/arm-acpi.txt).

La prise en charge de _Full DynTicks_ pour les invités [KVM](http://www.phoronix.com/scan.php?page=news_item&px=Linux-4.1-DynTicks-KVM-Guest) permet de ne plus réveiller le processeur à intervalles réguliers, et donc permet une meilleure économie d’énergie quand on a des machines virtuelles en fonctionnement.

### AMD Bulldozer


Amélioration de l’entropie pour AMD Bulldozer, ce qui permettra d’avoir des nombres aléatoires de meilleure qualité pour les opérations de chiffrement.

### Changement risqué pour x86
Beaucoup de changements ont été apportés aux appels système, interruptions, etc. pour x86.  Un gros effort a été mené pour clarifier et remplacer du code spaghetti en assembleur, vieux d’une décennie, par du C, ainsi que ses dépendances en C. Le code est maintenant plus simple, plus clair, plus compact et donc maintenable.
En raison de la nature des changements sur ces parties critiques, des [risques de problèmes peuvent apparaître](http://www.phoronix.com/scan.php?page=news_item&px=Linux-4.1-For-x86-ASM).

### Correction à chaud sur architecture S/390
Gestion préliminaire pour l’utilisation de correctifs à chaud du noyau pour l’architecture S/390. Suppression de la prise en charge du mode [31 bits](http://en.wikipedia.org/wiki/31-bit) autrefois nécessaire pour dépasser l’agaçante limite mémoire des 16 Mio.

### Équilibrage de charge
La surveillance de la charge par l’ordonnanceur a été retravaillée pour que la charge par processus soit indépendante de la vitesse du processeur. Cela devrait permettre de meilleures décisions pour l’équilibrage de charge selon les changements de fréquence, et améliorer la prise en charge des systèmes à processeurs asymétriques comme le récent _big.LITTLE_ où plusieurs types de cœurs se retrouvent dans le même processeur. La technologie _big.LITTLE_ permet de mixer des cœurs à très faible consommation avec des cœurs puissants s’activant à la demande en cas de forte sollicitation. Cela permet d’économiser de l’énergie.

### MIPS


L’architecture MIPS prend dorénavant en charge l’adressage XPA. Cela permet aux systèmes 32 bits d’accéder à toutes les adresses de la mémoire physique (40 bits).

## Développeurs

### Utilisation  de GCC 6

<expliquer pourquoi les devs noyau ont besoin de faire ça>


Le noyau Linux utilise actuellement un fichier d’entête `compiler-gccX.h` pour gérer efficacement les spécificités propres à chaque version majeure de GCC.

La possibilité de tout fusionner dans un seul `compiler-gcc.h` est discutée, pour ne pas créer un fichier par version de GCC dû au changement dans la politique de publication de GCC (une version par an). En effet, précédemment, chaque version majeure était due à une évolution marquante, et pouvait fortement affecter le noyau.

La nouvelle politique pour GCC est plus pragmatique, avec des versions plus incrémentales et régulières, permettant de profiter des avancées du projet, sans forcément remettre en question de manière profonde le logiciel et la manière de l’utiliser (raison première d’un fichier par version).

Il reste encore des changements à faire pour rendre le code compatible avec le compilateur [LLVM/Clang](http://llvm.linuxfoundation.org/index.php/Quick_Start_Guide) et la norme [C11](http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=51b97e354ba9fce1890cf38ecc754aa49677fc89).

### AIO


Les méthodes `aio_read()` et `aio_write()` ont été supprimées de la structure `file_operations`. Les (relativement) nouvelles méthodes `read_iter()` et `write_iter()` devraient être utilisées à la place.

### Débogage des systèmes de fichiers
Il y a une nouvelle cible « _log writes_ » pour la [carte des périphériques](https://fr.wikipedia.org/wiki/Carte_des_p%C3%A9riph%C3%A9riques) (_device mapper_) qui enregistre toutes les opérations d’écriture sur un périphérique bloc. Il est destiné au débogage des systèmes de fichiers. Voir `Documentation/device-mapper/log-writes.txt` pour les détails.

### User mode Linux
Linux en mode utilisateur _— User mode Linux —_ a vu la prise en compte du multitâche et de la gestion mémoire [_highmem_](https://www.kernel.org/doc/Documentation/vm/highmem.txt) supprimés. Aucune de ces fonctionnalités ne marchait correctement — voire ne fonctionnait tout court — et les deux étaient des fardeaux à maintenir.


### GPIO
Le nouveau système d’« accaparement » du système d’entrées‐sorties à usage général [GPIO](https://fr.wikipedia.org/wiki/General_Purpose_Input/Output) peut être utilisé pour câbler facilement — et de façon permanente — l’état d’une ligne GPIO spécifique sans nécessiter de pilote. Voir la documentation du [_correctif_](http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=6b516a1093006a39368dd11a5396be5bb00c99df) pour les détails.


### Outil perf
Comme d’habitude, l’outil de mesure de performance a vu une longue liste d’ajouts et d’améliorations ; voir le [correctif de la demande d’inclusion](http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=6c8a53c9e6a151fffb07f8b4c34bd1e33dddd467) pour les détails. Certaines des caractéristiques les plus importantes incluent la possibilité d’attacher des programmes BPF aux sondes noyau, la prise en compte de la fonctionnalité _trace_ des processeurs Intel à venir (« un traceur matériel sous stéroïdes »), la prise en compte de la fonction à venir de surveillance de la qualité de service du cache d’Intel, etc. ([Documentation Intel : _Processor Tracing_](https://software.intel.com/en-us/blogs/2013/09/18/processor-tracing)).

### Suppression du « domaine d’exécution » noyau
Le « domaine d’exécution » a été retiré du noyau. L’idée qui sous‐tendait cette fonctionnalité était de permettre l’importation de caractéristiques étrangères à Linux — des interfaces logicielles de noyaux d’autres systèmes UNIX —, mais elle n’a jamais été très utilisée, et n’a même jamais très bien fonctionné.


Voir l’article de _LWN.net_ : [_Remove execution domain support_](https://lwn.net/Articles/640083/).

### Minimisation de la taille du noyau
L’utilisation d’utilisateurs, de groupes et de « capacités » (_capabilities_) est optionnel. Cette option de configuration à la compilation est destinée aux tout petits systèmes embarqués qui n’en faisaient déjà pas usage, et qui peuvent donc être allégés de 25 Kio environ.

Voir l’article de _LWN.net_ :[Linux as a single-user system](http://lwn.net/Articles/631853/), [[correctif](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=2813893f8b197a14f1e1ddb04d99bce46817c84a)].

## Pilotes graphiques libres

### Direct Rendering Manager


La nouveauté principale pour la partie commune de tous les pilotes graphiques libres est l’ajout d’un allocateur de mémoire graphique « virtuelle » (c’est‐à‐dire, utilisant la mémoire centrale de l’ordinateur au lieu d’utiliser de la mémoire graphique dédiée). Cet allocateur réutilise l’interface [GEM](https://fr.wikipedia.org/wiki/Graphics_Execution_Manager) qui est l’interface d’allocation utilisée par presque tous les pilotes graphiques libres. Cette approche a pour unique intérêt d’aider les pilotes de rendu (_rasterisation_) logicielle tels que le pilote Mesa/Gallium LLVMpipe.

Grâce à la couche _Virtual GEM_, il devient possible à LLVMpipe de transférer avec DRI2 le résultat du rendu au serveur graphique X sans effectuer de copies. Cette couche devrait cependant être inutile dans le cas de Wayland et DRI3 qui préfèrent utiliser [_DMA-buf_](https://www.kernel.org/doc/Documentation/dma-buf-sharing.txt) pour des raisons de sécurité et de simplicité. DMA-buf étant plus générique que GEM, il a toujours été possible de partager de la mémoire centrale avec plusieurs pilotes. Cependant, DRI3 n’est pas encore considéré comme stable actuellement, ce qui est probablement la raison pour laquelle Zach Reizner a décidé de ressusciter le projet d’Adam Jackson, empaqueteur de la pile graphique pour Red Hat, [datant de 2012](http://lists.freedesktop.org/archives/dri-devel/2012-January/018113.html).

Le [correctif](http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=502e95c6678505474f1056480310cd9382bacbac) original permettait également de gérer le partage de tampons graphiques via PRIME (DMA-buf), cependant, plusieurs ingénieurs ont trouvé que Google avait décidé de se servir de cette interface d’une autre façon qui n’est pas sûre vis‐à‐vis de la cohérence des caches, surtout sur les processeurs ARM. Le résultat de la [discussion](http://lists.freedesktop.org/archives/dri-devel/2015-May/083160.html) est que VGEM doit respecter son objectif initial, qui est celui proposé par Adam en 2012, LLVMpipe sur DRI2. La gestion de PRIME a donc été supprimée.

Pour que cette fonctionnalité devienne utile, il reste cependant encore à modifier le pilote LLVMpipe pour enfin éviter les copies inutiles des images produites par LLVMpipe à destination du serveur graphique X, qui sont particulièrement lourdes avec les résolutions actuelles de nos moniteurs.

Le reste des modifications de la partie commune des pilotes graphiques libres n’est pas aussi intéressant. Pour plus d’informations, vous pouvez consulter les demandes d’intégration [DMA-buf](http://lkml.iu.edu/hypermail/linux/kernel/1504.3/00294.html), [panels](http://lists.freedesktop.org/archives/dri-devel/2015-April/080694.html) et  [DRM](http://lists.freedesktop.org/archives/dri-devel/2015-April/081457.html).

### AMD/ATI

#### Pilote de rendu (pilote radeon)

Dans cette nouvelle version, le pilote _radeon_ ajoute une gestion expérimentale du transport multiflux (MST) sur [DisplayPort 1.2](https://fr.wikipedia.org/wiki/DisplayPort#DisplayPort_1.2). Cela permet de faire transiter des flux (images, son et données) pour différents écrans via une seule sortie (et un seul câble) DisplayPort. Cette gestion a été ajoutée par Dave Airlie (Red Hat) pour les processeurs Cayman et (en théorie) les processeurs plus récents. Pour activer cette gestion, vous devrez utiliser l’option noyau `radeon.radeon_mst=1`.

Une autre fonctionnalité intéressante a été ajoutée pour ceux d’entre nous qui connectent leur carte AMD à une télévision. Les télévisions sont en effet différentes des moniteurs pour ordinateurs, car elles ne reçoivent en théorie que des couleurs RVB dans la plage 16-235 au lieu de 0-255. Cela veut dire que les couleurs 0-16 seront toutes considérées comme absence d’une couleur et 235-255 seront considérées comme sa présence complète. Cette gestion [a été demandée durant l’été 2014](https://bugs.freedesktop.org/show_bug.cgi?id=83226) par plusieurs utilisateurs. Si vous voulez essayer, il vous faudra invoquer `xrandr` de cette façon `xrandr --output <sortie> --set output_csc tvrgb`.

Le pilote _radeon_ exporte désormais beaucoup plus d’informations concernant l’état du processeur graphique. Ces informations sont destinées à être exposées par l’afficheur tête haute de Gallium ou les extensions OpenGL permettant d’exposer les compteurs de performance. Ces informations supplémentaires concernent les horloges mémoire et processeur graphique, ainsi que l’état des différents moteurs d’exécution du processeur graphique (2D, 3D, décodage vidéo et DMA).

Pour plus d’informations sur les nouveautés du pilote _radeon_, vous pouvez consulter la [demande d’intégration _radeon_](http://lists.freedesktop.org/archives/dri-devel/2015-March/079634.html).

#### Pilote HSA (pilote amdkfd)


Le pilote _amdkfd_, qui permet de faire communiquer plus rapidement le processeur graphique et le processeur, permet désormais de gérer plusieurs pilotes graphiques en même temps. C’est nécessaire afin de gérer le futur pilote graphique qui sera prochainement intégré au noyau et qui sera l’unique pilote Linux capable de gérer les nouveaux [APU](https://fr.wikipedia.org/wiki/Accelerated_processing_unit) d’AMD (Carrizo).

Pour plus d’informations sur les nouveautés du pilote _amdkfd_, vous pouvez consulter la [demande d’intégration _amdkfd_](http://lists.freedesktop.org/archives/dri-devel/2015-March/079981.html).

### Intel (pilote i915)


Peu de nouveautés visibles pour les utilisateurs du pilote _i915_. Les nouveautés principales concernent l’affichage. La première est la gestion dynamique de la fréquence de rafraîchissement (DRRS) pour les plates‐formes qui le prennent en charge. La seconde est la gestion matérielle de la lecture des tampons graphiques utilisant le _Y-tiling_, avec une rotation de 90° et 270°, en plus des 0° et 180° déjà disponibles sur les plates‐formes avant le Skylake.

Beaucoup de travail de réagencement du code est également en cours. Le travail principal continue sur la gestion atomique du mode graphique avec l’ajout de l’infrastructure qui va faciliter la migration de la gestion actuelle à la gestion atomique.

Du côté de PPGTT (mémoire virtuelle allouée par client graphique), beaucoup d’efforts sont faits afin de gérer des espaces d’adressage supérieurs à 32 bits (4 Gio). En effet, activer un espace d’adressage de 48 bits utiliserait beaucoup de mémoire rien que pour allouer la table de pagination. Du coup, Ben Widawsky travaille à faire l’allocation de cette table de façon dynamique. Espérons que Linux 4.2 apportera la gestion des grands espaces d’adressage qui permettront au processeur graphique d’accéder à toute la mémoire d’un processus 64 bits sans avoir à copier inutilement des zones mémoire ou faire des opérations de réagencement des MMU). Si vous êtes intéressés par PPGTT, vous pouvez consulter les articles de Ben Widawsky sur le sujet, qui expliquent en détail l’objectif derrière PPGTT ([_1_](https://bwidawsk.net/blog/index.php/2014/06/the-global-gtt-part-1/), [_2_](https://bwidawsk.net/blog/index.php/2014/06/aliasing-ppgtt-part-2/), [_3_](https://bwidawsk.net/blog/index.php/2014/07/true-ppgtt-part-3/) et [_4_](https://bwidawsk.net/blog/index.php/2014/07/future-ppgtt-part-4-dynamic-page-table-allocations-64-bit-address-space-gpu-mirroring-and-yeah-something-about-relocs-too/)).

Pour finir, les développeurs _i915_ essayent de fournir l’accélération graphique aux machines virtuelles Xen sans avoir à passer par une virtualisation complète. Les machines virtuelles auront un pilote (appelé XenGT) qui communiquera avec un autre pilote sur l’hôte via des _hypercalls_, afin de transmettre les différentes commandes. Le résultat devrait être très performant et proche de ce qu’[essaye de faire Ben Skeggs pour le pilote Nouveau](https://linuxfr.org/news/sortie-du-noyau-linux-4-0#nvidia-pilote-nouveau).

Si vous voulez en savoir plus, vous pouvez consulter l’habituel [compte‐rendu détaillé](http://blog.ffwll.ch/2015/04/neat-drmi915-stuff-for-41.html) des modifications de Daniel Vetter (mainteneur du pilote _i915_).

### NVIDIA (pilote nouveau)


Du côté du pilote libre pour les processeurs graphiques NVIDIA, l’ingénieur Alexandre Courbot de NVIDIA a ajouté la gestion de l’[IOMMU](https://en.wikipedia.org/wiki/IOMMU) pour les processeurs graphiques GK20A, que l’on peut trouver dans les systèmes monopuces Tegra K1.

Ben Skeggs a également écrit les microcodes nécessaires à la gestion matérielle des contextes graphiques pour la première version des processeurs Maxwell, les puces GM107. Ces microcodes sont essentiels afin de prendre en charge l’accélération 2D et 3D et le décodage vidéo. Les puces suivantes ont également reçu des modifications afin de pouvoir charger ces microcodes, mais aucune version libre ne peut être écrite, car ces derniers nécessitent un microcode signé par NVIDIA. Ce dernier devraient redistribuer très prochainement les différents microcodes dans le projet `linux-firmware`.

Avec un peu de chance, NVIDIA redistribuera également les microcodes nécessaires à l’encodage et au décodage vidéo, ainsi que la documentation des interfaces, ce qui devrait réduire le temps de développement pour ajouter la gestion d’une nouvelle puce ! On se consolera comme on peut pour la perte de liberté, et un travail d’audit du code sera encore nécessaire pour garantir qu’il n’y a pas de portes dérobées.

### Pilotes graphiques pour systèmes monopuces

#### Qualcomm (pilote msm)


Le pilote _msm_ pour les processeurs graphiques Qualcomm gagne la gestion des liens DSI et double DSI, ainsi que la gestion du vol de mémoire, ce qui permet de faire une transition sans à‐coups depuis l’animation de chargement (_splash screen_). La gestion des nouveaux processeurs graphiques Snapdragon 410 fait également son apparition.

Pour plus d’informations, vous pouvez consulter la [demande d’intégration _msm_](http://lists.freedesktop.org/archives/dri-devel/2015-April/080508.html).

#### NVIDIA (pilote tegra)


La nouveauté principale du pilote NVIDIA _tegra_ à destination des systèmes monopuces Tegra (2, 3 et 4) est la possibilité d’utiliser les compteurs matériels de suivi du rafraîchissement vertical grâce aux points de synchronisation proposés par le pilote et le bloc _host1x_. Cela devra permettre d’implémenter une gestion plus efficace de la commutation de page (_page flipping_).

Pour plus d’informations, vous pouvez consulter la [demande d’intégration _tegra_](http://lists.freedesktop.org/archives/dri-devel/2015-April/080692.html).

#### Renesas (pilote rcar-du)


Le pilote Renesas a reçu beaucoup de modifications de la part de Laurent Pinchart, afin de gérer la gestion atomique du mode graphique !

Pour plus d’informations, vous pouvez consulter [les demandes](http://lists.freedesktop.org/archives/dri-devel/2015-March/078589.html) [d’intégration _rcar-du_](http://comments.gmane.org/gmane.comp.video.dri.devel/125918).

#### Autres


Les pilotes Samsung (_exynos_) et Texas Instruments (_omap_) ont tous les deux profité d’une révision de l’architecture du code et de corrections des bogues en vue de gérer la gestion atomique du mode graphique, mais le code n’est pas encore prêt.

Pour plus d’informations, vous pouvez consulter les demandes d’integration [_exynos_](http://lists.freedesktop.org/archives/dri-devel/2015-April/081138.html) et [_omap_](http://permalink.gmane.org/gmane.comp.video.dri.devel/126243).

## Sécurité


Lorsque le noyau essaie d’accéder à des adresses qui ne sont pas directement accessibles, la MMU génère une erreur de pagination — _page fault exception_ — et exécute le gestionnaire d’erreur de pagination. Lors de l’accès à des zones mémoires contrôlées par un processus en espace utilisateur, ce gestionnaire vérifie que le code effectuant cet accès fait bien partie d’un tableau listant les parties dans le noyau qui ont effectivement le droit d’accéder à ce type de zone mémoire. Ce tableau contient donc principalement toutes les invocations des fonctions de type `copy_*_user()`.

Ce mécanisme permet au noyau d’accéder de façon sûre aux données en espace utilisateur à l’aide d’une interface restreinte (les fonctions `copy_*_user()`) sans avoir à vérifier explicitement l’accès à tous les pointeurs avant de les déréférencer.

Chaque module contient son propre tableau listant ces exceptions. À partir de cette version du noyau, le chargeur de module vérifiera activement que toutes les entrées de ce tableau pointent bien vers du code inclus dans le module. Toute entrée pointant à l’extérieur générera une erreur [[correctif](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=050e57fd5936eb175cbb7a788252aa6867201120)], [Documentation/x86/exception-tables.txt : Kernel level exception handling in Linux](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/tree/Documentation/x86/exception-tables.txt).

### Audit


Une situation de compétition potentielle dans le code d’audit pouvait tronquer le rapport d’audit après le champ `comm` et donc provoquer la perte d’informations. Pour de plus amples informations, voir le [correctif](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=5deeb5cece3f9b30c8129786726b9d02c412c8ca).

### SELinux


La table de hachage contenant la politique SELinux dans le noyau a été convertie en « tableau flexible » (flex_array). Il utilise un nouvel algorithme de hachage produisant une meilleure répartition. Le nombre d’entrées par défaut a également été augmenté. Ces modifications devraient réduire l’impact de la recherche d’une règle dans la politique SELinux dans le noyau et donc réduire globalement l’impact de SELinux. Pour de plus amples informations, voir les correctifs [1](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=ba39db6e0519aa8362dbda6523ceb69349a18dc3), [2](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=33ebc1932a07efd8728975750409741940334489) et [3](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=cf7b6c0205f11cdb015384244c0b423b00e35c69).

Certaines commandes netlink n’étaient pas mentionnées dans la table utilisée par SELinux à cause d’un oubli lors de leur introduction dans le noyau. Ces correctifs corrigent le tir et l’un d’entre eux ajoute une vérification à la compilation pour que la situation ne se reproduise plus. Pour de plus amples informations, voir les correctifs [1](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=5bdfbc1f19d047a182d2bab102c22bbf2a1ea244), [2](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=387f989a60db00207c39b9fe9ef32c897356aaba), [3](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=2b7834d3e1b828429faa5dc41a480919e52d3f31), [4](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=5e6deebafb45fb271ae6939d48832e920b8fb74e), [5](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=5b5800fad072133e4a9c2efbf735baaac83dec86), [6](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=b0b59b0056acd6f157a04cc895f7e24692fb08aa), [7](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=8d465bb777179c4bea731b828ec484088cc9fbc1), [8](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=bd2cba07381a6dba60bc1c87ed8b37931d244da1) et [9](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=cf890138087a6da2f56a642acb80476370b04332).

### SMACK


Les _sockets_ ouvertes par des fils d’exécution dans le noyau sont correctement nommées. Pour de plus amples informations, voir le [correctif](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=7412301b76bd53ee53b860f611fc3b5b1c2245b5).

L'appel système `keyctl` avec l'action `KEYCTL_GET_SECURITY` retourne maintenant le contexte de sécurité [[correctif](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=7fc5f36e980a8f4830efdae3858f6e64eee538b7)].

Le mainteneur du module de sécurité SMACK a ajouté à contre cœur un mode « mise en place » qui peut être utilisé pour déboguer la configuration et la politique SMACK. « C'est finalement disponible, c'est dangereux, mais il semble que beaucoup de développeurs ne sachent pas faire autrement, donc j'ai mis ça en place. J'ai essayé de rendre ce mode aussi sûr que possible, mais c'est de toute façon impossible vu que cela reste une tronçonneuse ». Pour de plus amples informations, voir le [correctif](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=bf4b2fee99799780ea3dbb6d79d1909b3e32be13).

### Liste non exhaustive des vulnérabilités corrigées


* CVE-2015-4178 [Mitre](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-4178) [[correctif](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=820f9f147dcce2602eefd9b575bbbd9ea14f0953)] ;
* CVE-2015-4177 [Mitre](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-4177) [[correctif](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=cd4a40174b71acd021877341684d8bb1dc8ea4ae)] ;
* CVE-2015-4176 [Mitre](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-4176) [[correctif](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=e0c9c0afd2fc958ffa34b697972721d81df8a56f)] ;
* CVE-2015-4001 [NVD](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2015-4001), [Mitre](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-4001), CVE-2015-4002 [NVD](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2015-4002), [Mitre](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-4002), CVE-2015-4003 [NVD](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2015-4003), [Mitre](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-4003), CVE-2015-4004 [NVD](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2015-4004), [Mitre](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-4004) : [Annonce sur la LKML par Jason A. Donenfeld](https://lkml.org/lkml/2015/5/13/739), [Annonce sur oss-sec](http://seclists.org/oss-sec/2015/q2/446), [Documentation du driver OZWPAN](https://www.kernel.org/doc/readme/drivers-staging-ozwpan-README) [correctifs : [1](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=d114b9fe78c8d6fc6e70808c2092aa307c36dc8e), [2](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=b1bb5b49373b61bf9d2c73a4d30058ba6f069e4c), [3](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=04bf464a5dfd9ade0dda918e44366c2c61fce80b), [4](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=9a59029bc218b48eff8b5d4dde5662fd79d3e1a8)] ;
* CVE-2015-3339 [NVD](https://web.nvd.nist.gov/view/vuln/detail?vulnId=CVE-2015-3339), [Mitre](http://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2015-3339) [[correctif](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=8b01fc86b9f425899f8a3a8fc1c47d73c2c20543)] ;
* (?) [[correctif](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=51dfcb076d1e1ce7006aa272cb7c4514740c7e47)] ;
* Pas de CVE : Fuite d'information [[correctif](http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=33cf7c90fe2f97afb1cadaa0cfb782cb9d1b9ee2)].

### Nouveau matériel pris en charge

* Générateur de nombres aléatoires : Altus Metrum ChaosKey [[correctif](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=66e3e591891da9899a8990792da080432531ffd4)].

## Réseau


### eBPF


eBPF est un système universel dans le noyau de machine virtuelle, désormais utilisé pour la classification et le routage des paquets. Cela permet d’avoir plus de débit et moins de latence, particulièrement sur les routeurs. 

Plus d'informations dans les correctifs ([1](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=e2e9b6541dd4b31848079da80fe2253daaafb549), [2](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=a8cb5f556b567974d75ea29c15181c445c541b1f)) et l'[article de Phoronix](http://www.phoronix.com/scan.php?page=news_item&px=More-eBPF-Linux-4.1-Kernel).

### Routage des paquets en utilisant le mécanisme « multiprotocol label switching » (MPLS)

Le mécanisme de routage MPLS ([MultiProtocol Label Switching](https://fr.wikipedia.org/wiki/Multiprotocol_Label_Switching)) est maintenant géré par Linux. Correctifs : [1](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=0189197f441602acdca3f97750d392a895b778fd), [2](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=0189197f441602acdca3f97750d392a895b778fd), [3](https://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=a2519929aba78e8cec7955d2c2a0c1e230d1f6e6).

### Implémentation de la RFC 7217 (IPv6)

La [RFC 7217](https://tools.ietf.org/html/rfc7217) vient d’être implémentée dans Linux ce qui devrait permettre de garder une adresse IPv6 anonyme (l'adresse MAC n'est pas divulguée dans l'adresse IP) tout en gardant la propriété importante de stabilité de l'adresse lors de la mobilité. Pour plus d'informations, veuillez consulter la [RFC 7217](https://tools.ietf.org/html/rfc7217).

## Systèmes de fichiers


### Pilote pour les périphériques dits à mémoire persistante
Le pilote basique de mémoire persistante a été incorporé, améliorant la prise en compte dans le noyau des périphériques à mémoire non-volatile de grande taille.
[Article LWN.net : Persistent memory support progress](http://lwn.net/Articles/640113/).

### F2FS

F2FS dispose maintenant du cache en mémoire `extent_cache`. La fonctionnalité `fs_shutdown` permet de faire des tests de coupure d’alimentation pour fiabiliser le comportement en cas de coupure à chaud. Dorénavant, le chemin des liens symboliques est stocké _inline_, c’est-à-dire directement dans le nœud d’index (inode). F2FS permet déjà de stocker des petits fichiers directement dans ces derniers (voir [Enable f2fs support inline data](https://lwn.net/Articles/573408/)). Les liens symboliques étant un cas spécifique de petit fichier (fichier contenant un lien), ce traitement ne s’appliquait jusqu’alors pas à eux. On notera par ailleurs l’activation par défaut du stockage _inline_ à partir de cette version.
Diverses améliorations côté gestion de l’énergie, point important vu l’utilisation de F2FS dans nos téléphones.


Pour plus d'informations, vous pouvez consulter l'[article de Phoronix](http://www.phoronix.com/scan.php?page=news_item&px=Linux-4.1-F2FS).


### BLK-MQ
La gestion des _multi-queues_ [a été améliorée](http://www.phoronix.com/scan.php?page=news_item&px=Linux-4.1-Block-Core-BLK-MQ), surtout avec le multi-CPU.

### EXT4

[_ext4_ est maintenant capable de chiffrer](http://lwn.net/Articles/639427/) directement les fichiers et dossiers du système de fichiers. Cette nouvelle fonctionnalité permet de se passer de [_dm-crypt_](https://fr.wikipedia.org/wiki/Dm-crypt) ou [_eCryptfs_](http://lwn.net/Articles/156921/), suivant les cas.

### XFS


Lors d’un renommage, la source n’est plus supprimée mais remplacée par des blancs (`RENAME_WHITEOUT`). Cela devrait permettre son utilisation avec le système d'union de système de fichier overlayfs.


Nouveau aussi dans XFS, la prise en charge de l’option `FALLOC_FL_INSERT_RANGE` par `fallocate()`, permettant aux applications d’[insérer des trous dans un fichier](http://lwn.net/Articles/629965/).

Les superblocs par CPU ont été remplacés par des compteurs génériques.


Nouveau verrou `inode mmap` sur les erreur de page.


Une réécriture de la soumission des I/O directes pour mieux faire attention aux corruptions de données lors des écritures


Amélioration du log des messages


Et les traditionnelles corrections de bogues et nettoyage.

### RAID 5/6 pour le Raid Logiciel


Les blocs de 4K sont maintenant gérés. Le cache de bande est maintenant dynamique. Le raid6 peut maintenant faire des cycles lecture/modification/écriture avec de meilleures performances sur des grappes de 6 disques ou plus.


### RAID 1


Le sous-système MD (RAID) peut dorénavant gérer les matrices RAID 1 de façon distribuée sur un cluster. Le code correspondant est actuellement marqué comme expérimental, mais il est de toute évidence proche d’un état de production.

### Device mapper


Le gestionnaire de périphériques peut maintenant fonctionner comme un périphérique bloc à files multiples, augmentant les performances. Cette fonctionnalité est actuellement désactivée par défaut, mais peut être activée avec la variable de configuration `CONFIG_DM_MQ_DEFAULT`.

### BTRFS

Les systèmes de fichiers de plus de 20 To n’étaient pas gérés correctement à cause d’un problème de gestion de l’espace libre, cela est maintenant corrigé. Des corrections pour la suppression de fichiers de plus de 3 To ont également été apportées.
Enfin, un bogue empêchant le passage d’un niveau de raid à un autre a de même été corrigé.

### Tracefs
Steven Rostedt vient d’intégrer un nouveau système de fichiers : [TraceFS](http://lkml.iu.edu/hypermail/linux/kernel/1504.1/03930.html). Certains administrateurs système ont remonté qu’il y avait un souci à être obligé d’utiliser Debugfs pour pouvoir activer les fonctionnalités de « tracing » du noyau. Pour des raisons de sécurité, certains interdisent simplement l’utilisation de Debugfs.

Tracefs apporte la possibilité d’utiliser les capacités de « tracing » sans l’utilisation de Debugfs et ceci sans casser la compatibilité avec les outils existants.

### ZRAM

Le périphérique bloc « zram » peut maintenant effectuer la compression de blocs de données. Voir [l'article LWN sur le sujet](http://lwn.net/Articles/545244/).


## Meilleure gestion des erreurs pour les disques
Le standard ATA de commandes de gestion des disques a une nouvelle spécification ACS-4. Elle permet grâce à la commande _NCQ Autosense_ d’obtenir le même niveau d’information sur les erreurs qu’en SCSI. Le noyau 4.1 [implémente](http://www.phoronix.com/scan.php?page=news_item&px=Linux-4.1-NCQ-Autosense) cette commande, qui n’est pour l’instant reconnue que par une poignée de nouveaux disques durs.

## Virtualisation


###KVM


Comme d’habitude, la demande d’intégration se fait en deux fois, une pour [les PowerPC](https://lkml.org/lkml/2015/4/23/375) et une pour [les autres architectures](https://lkml.org/lkml/2015/4/10/550).

- s390 (mainframe IBM) : prise en charge des instructions SIMD, le code de gestion des interruptions a été retravaillé afin de pouvoir récupérer la liste des interruptions et de pouvoir en définir avec des ioctl, ceci afin de permettre la migration d’invité d’un hôte à l’autre. [Les clefs de stockage](http://www-01.ibm.com/support/knowledgecenter/zosbasics/com.ibm.zos.zconcepts/zconcepts_95.htm) qui permettent de protéger l’accès aux données peuvent être maintenant définies et récupérés dans un invité via de nouvelles ioctl.
- MIPS : prise en charge des instructions FPU et SIMD.
- x86 : principalement des corrections de bugs, il y a eu un débat sur le fait que les développeurs KVM se sont permis d’aller mettre du code dans l’ordonnanceur, zone du noyau qui appartient à un autre mainteneur, qui plus est, ce dernier n’était pas du tout content du code. Finalement, le code a été écrit d’une autre façon pour qu’il n’y ait pas besoin de modifier autre chose que KVM.
- ARM : des corrections pour la migration à chaud, prise en charge des `irqfd` (ce qui permet d’injecter des interruptions dans l’invité via un descripteur de fichier *eventfd*) et [ioeventfd](http://git.kernel.org/cgit/linux/kernel/git/torvalds/linux.git/commit/?id=d34e6b175e61821026893ec5298cc8e7558df43a) (qui permet de faire l’asynchrone pour certaines opérations d’entrée/sortie pour lesquelles une opération synchrone dans un invité est très coûteuse comme une demande d’opération DMA) ainsi que le « *page aging* ».
- PowerPC : amélioration du débogage, légère amélioration des performances et nettoyage du code pour les processeurs de type Book3S.

### Xen

- Ajout d’un pilote APIC pour gérer plus de 255 processeurs virtuels dans les invités.
- Amélioration des performances pour les opérations de migration, sauvegarde et restauration de l’état.
- Prise en charge de la sauvegarde/restauration pour le pilote scsiback/front
- Mise en place de l’infrastructure pour avoir le *[xenbus](http://wiki.xen.org/wiki/XenBus)* sur plusieurs pages.

### VirtIO


Le sous-système `virtio` a un nouveau pilote `virtio-input`. Son travail est de collecter et faire suivre les événements des périphériques d’entrée vers un périphérique virtuel.

# Le bilan en chiffres


En ce qui concerne les statistiques du cycle de développement du noyau 4.1, le site _LWN.net_ a publié son traditionnel [article récapitulatif](https://lwn.net/Articles/646942/).

En nombre de modifications, on se situe à 11 664, soit environ 1 500 modifications de plus que la version précédente du noyau, mais reste en dessous de la moyenne de ces derniers noyaux. Cette nouvelle version a ajouté 486 000 lignes de code alors qu’elle en a supprimé 286 000, ce qui a conduit à une augmentation nette de 200 000 lignes. Le nombre de contributeurs s’approche quant à lui au moins à 1 492 auteurs. Il est possible que lors de la sortie, plus de 1 500 auteurs aient contribué ce qui est un nouveau record !

Pour changer, le développeur ayant fait le plus de modifications travaille au nettoyage des pilotes Comedi. Cependant, ce n’est pas l’habituel Hartley Sweeten, mais Ian Abbott qui remporte la palme cette fois. Du côté des développeurs ayant le plus modifié de lignes, Jie Yang se retrouve sur la première marche par son travail de réorganisation du pilote audio Intel.

Au moins 215 entreprises ont participé à l’élaboration de ce noyau. En tête, on retrouve encore Intel, qui a effectué 11,2 % des changements que l’on peut trouver dans cette nouvelle version. En deuxième place, Red Hat a contribué pour 9,2 % des changements. Il est cependant important de noter que les développeurs sans affiliation ont effectué 9 % des modifications, soit juste un peu moins que Red Hat, alors que les personnes non identifiées ont écrit 8,1 % des modifications. On peut donc dire que, bien que le noyau soit majoritairement écrit par des employés d’entreprises, les contributeurs indépendants sont toujours les bienvenus et sont même une majorité !

# Appel à volontaires


Cette dépêche est rédigée par plusieurs contributeurs dont voici la répartition :

|                             | Mainteneur                  | Contributeur(s)
----------------------------- | -------------------------------------------------- | ---------------
**La phase de test**          | Aucun | [eggman](//linuxfr.org/users/eggman)  | 
**Arch**                      | [Romain Perier](//linuxfr.org/users/rperier) | 
**Développeurs**              | Aucun |
**Pilotes graphiques libres** | [Martin Peres](//linuxfr.org/users/mupuf)    |
**Réseau**                    | Aucun | [BRULE Herman (alpha_one_x86)](//linuxfr.org/users/alpha_one_x86) |
**Systèmes de fichiers**      | Aucun | [BRULE Herman (alpha_one_x86)](//linuxfr.org/users/alpha_one_x86),[Mali](http://linuxfr.org/users/mali) |
**Sécurité**                  | [Timothée Ravier](//linuxfr.org/users/siosm) |
**Virtualisation**            | [Xavier Claude](//linuxfr.org/users/claudex) |
**Édition générale**          | Aucun | [Martin Peres](//linuxfr.org/users/mupuf), [eggman](//linuxfr.org/users/eggman), [Timothée Ravier](//linuxfr.org/users/siosm)

Un peu de vocabulaire :


 * le mainteneur d’une section de la dépêche est responsable de l’organisation et du contenu de sa partie, il s’engage également à l’être dans le temps jusqu’à ce qu’il accepte de se faire remplacer ;
 * un contributeur est une personne qui a participé à la rédaction d’une partie d’une section de la dépêche, sans aucune forme d’engagement pour le futur.


Malgré cette équipe importante, beaucoup de modifications n’ont pas pu être expliquées par manque de temps et de volontaires.


> Nous sommes particulièrement à la recherche de mainteneurs pour les sections _Édition générale_, _Systèmes de fichiers_ et _Réseau_, les précédents n’ayant pas donné de signes de vie pendant la rédaction des dernières dépêches.

Si vous aimez ces dépêches et suivez tout ou partie de l’évolution technique du noyau, veuillez contribuer dans votre domaine d’expertise. C’est un travail important et très gratifiant qui permet aussi de s’améliorer. Il n’est pas nécessaire d’écrire du texte pour aider, simplement lister les _commits_ intéressants dans une section aide déjà les rédacteurs à ne pas passer à côté des nouveautés. La page wiki _[[[Rédiger des dépêches noyau]]]_ signale quelques possibilités pour aider à la rédaction et s’y impliquer (ce que tout inscrit peut faire, ne serait‐ce que traduire\^Wsynthétiser les annonces de RC). Essayons d’augmenter la couverture sur les modifications du noyau !
