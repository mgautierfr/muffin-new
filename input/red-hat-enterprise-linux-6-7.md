menu-position: 4
title:   Red Hat Enterprise Linux 6.7
id: redhat
css-template: article.css
color1: #79db32
index-style: pure-u-1 pure-u-md-1-3
authors: Nils Ratusznik
         Yvan Munoz, BAud, palm123, Nÿco, patrick_g, eggman, Stéphane Aulery et Benoît Sibaud
date:    2015-05-06T10:17:16+02:00
license: CC by-sa
tags:    hat, red, redhat, rhel6 et rhel
---


Red Hat a annoncé le 22 juillet dernier la version 6.7 de Red Hat Enterprise Linux (RHEL), distribution commerciale destinée aux professionnels et aux entreprises. Pour rappel, RHEL 6 existe depuis novembre 2010 et, même si RHEL 7 est disponible depuis le mois de juin 2014, cette version est toujours maintenue.

![RHEL 6](http://www.sistina.com/g/rhel/rhel6/RHEL6_logo.png)

Cette version 6.7 apporte malgré tout des améliorations, principalement du côté de l'authentification centralisée et de la virtualisation. Red Hat avait annoncé la version bêta près de 3 mois auparavant, soit le [6 mai dernier](https://www.redhat.com/en/about/blog/red-hat-enterprise-linux-67-beta-now-available).

Une sélection des nouveautés se trouve en deuxième partie de dépêche.

----

[Red Hat](http://www.redhat.com)
[DLFP : Red Hat Enterprise Linux 7.1](http://linuxfr.org/news/red-hat-enterprise-linux-7-1)
[DLFP : Red Hat Enterprise Linux 6.6](http://linuxfr.org/news/red-hat-enterprise-linux-6-6)
[DLFP : Red Hat Enterprise Linux 5.11](http://linuxfr.org/news/red-hat-enterprise-linux-5-11)
[Annonce de RHEL 6.7 sur le site de Red Hat](http://www.redhat.com/en/about/press-releases/red-hat-joins-platform-stability-and-open-innovation-latest-version-red-hat-enterprise-linux-6)
[Notes de version](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/6.7_Release_Notes/index.html)
[Notes techniques](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/6.7_Technical_Notes/index.html)

----

# Authentification et interopérabilité

Ces deux thèmes sont souvent regroupés ensemble, car c'est l'un des axes de travail de Red Hat ces derniers temps, semble-t-il : améliorer la compatibilité avec les solutions Microsoft. Pour RHEL 6.7, c'est l'authentification qui est concernée, en particulier côté [SSSD](https://fedorahosted.org/sssd/) (composant d'authentification centralisée), qui gagne non seulement la possibilité d'utiliser un UPN (User Principal Name) comme identifiant pour s'authentifier sur un système, mais aussi d'utiliser les GPO (Group Policy Objects) stockées dans un annuaire Microsoft Active Directory (abordé dans [RHEL 7.1](//linuxfr.org/news/red-hat-enterprise-linux-7-1#authentification-et-interop%C3%A9rabilit%C3%A9)).

Moins orientée Microsoft, une autre amélioration de SSSD concerne la notification d'expiration des mots de passe : celui-ci est maintenant capable de signaler qu'un mot de passe a expiré même si l'authentification a eu lieu sans mot de passe. Cela peut par exemple être le cas lors d'une connexion SSH par clé.

# Haute-disponibilité

Le chapitre haute-disponibilité comporte deux nouveautés intéressantes : la première concerne `Pacemaker`, qui est maintenant capable de prendre en compte le serveur web Nginx en tant que ressource. Il est donc possible d'utiliser Nginx en configuration haute-disponibilité entièrement "à la Red Hat".

La deuxième nouveauté concerne le _fencing_, qui permet de cloisonner une ressource indisponible, parfois en effectuant un redémarrage électrique du serveur hébergeant la ressource en question. Plusieurs nouveaux agents apparaissent dans RHEL 6.7, mais l'un d'entre eux concerne le module [iLO de Hewlett-Packard](http://en.wikipedia.org/wiki/HP_Integrated_Lights-Out) : _fence_ilo_ssh_. Plus besoin de se connecter à l'OS pour redémarrer le serveur !

# Développement

Bien que cette dépêche ne contienne pas de section sur le matériel, la première nouveauté sur le développement concerne du matériel, en particulier les processeurs Intel. Les dernières générations (certains Core, Pentium et Celeron, ainsi que les Xeon v3) disposent en effet d'une fonctionnalité nommée PAPI (Performance Application Programming Interface), dorénavant prise en charge par RHEL 6.7


La bibliothèque C a été améliorée pour éviter un crash durant un diagnostic, ce qui empêche une application de planter pendant l'affichage des informations après une corruption mémoire par exemple.


SystemTap passe en version 2.7 ! Cette mise à jour contient de nombreuses améliorations, comme l'activation et la désactivation à la volée de sondes, la prise en charge de multiples scripts, ainsi que des nouvelles sondes.


Du côté d'OProfile, c'est le _memory mapping_ qui est amélioré, dans le cas d'utilisation de _huge pages_ dans du code Java JIT.

# Environnement de bureau

Le paquet scap-workbench fait son apparition dans RHEL 6 ! Déjà présent dans RHEL 7, cet outil graphique (Qt 4) facilite le test d'une machine pour vérifier sa conformité à des pratiques de sécurité définies via OpenSCAP.


Côté performances, PCP (Performance Co-Pilot) a été mis à jour en version 3.10.2. Cela apporte entre autres de nouvelles métriques noyau (mémoire, vCPU, device mapper, NFS 4.1, cgroups), mais aussi la prise en charge de Python 3 et d'extensions d'API en Python. On notera aussi la compression des archives journalières au format [xz](https://fr.wikipedia.org/wiki/XZ_%28format_de_fichier%29).

# Stockage

 ⁠ De nouvelles options ont été ajoutées à UDEV, permettant aux administrateurs d'écrire plus facilement des règles de montages, comme par exemple monter les clefs USB en lecture seule.

Ajout de `iscsi.safe_logout`, empêchant toutes tentatives de déconnexion d'une session ISCSI, lorsque des disques de ce type sont montés. 

De nombreuses commandes LVM acceptent la sélection de critères, combinaison entre des opérateurs de comparaison `= != >` (..), des opérateurs logiques `&& ||` (..) et des opérateurs de groupes `[ { (` (..) Le « Guide de l'Administrateur LVM » a été [mis à jour](http://man7.org/linux/man-pages/man8/lvm.8.html) ([ici](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Logical_Volume_Manager_Administration/index.html) en version beta) en conséquence.

Toujours à propos de LVM, la fonctionnalité de cache est dorénavant pleinement prise en charge : elle permet d'utiliser un stockage petit mais rapide en tant que cache pour un disque plus grand et certainement plus lent. La note technique renvoie vers la page de manuel de [lvmcache(7)](http://man7.org/linux/man-pages/man7/lvmcache.7.html) pour plus d'informations.

Multipath suit la tendance en permettant maintenant de découper sa configuration en plusieurs fichiers précis. L'option _config\_dir_ est disponible dans le fichier central de configuration, permettant de définir un dossier dans lequel tout les .conf seront pris en compte. 

L'attente par défaut de 300s avant que Multipath ne prenne en compte une inaccessibilité matérielle peut donner une impression de service défaillant. Les options "delay_watch_checks" et "delay_wait_checks" ont été ajoutées, empêchant ainsi les matériels qui bagotent d'être ré-utilisés immédiatement à leurs retours en ligne. Les vérifications de délais "watch" & "wait" permettent de définir des seuils avant de déclarer ces matériels revenus en ligne comme réellement fiables et utilisables.

# Virtualisation


Les machines virtuelles KVM utilisent l'outil *kvm-clock* comme source de temps pour synchroniser leur horloge avec celle de l'hyperviseur après une veille prolongée. Dans ce cas, les machines virtuelles RHEL 6 avaient quelques difficultés à se synchroniser correctement. En conséquence, *kvm-clock* a été mis à jour pour que cette synchronisation soit plus fiable.


Dans le jeu de celui qui en fait le plus, KVM est maintenant capable de gérer 240 processeurs virtuels (vCPU) par machine virtuelle.


Cette nouvelle version de RHEL 6 est bien entendu disponible en tant qu'image à déployer comme conteneur ou comme machine virtuelle. L'annonce ajoute que les hôtes certifiés pour cela sont Red Hat Enterprise Linux 7, Red Hat Enterprise Linux Atomic Host, et OpenShift Enterprise 3.

# Noyau

Certaines versions de gcc, ainsi que le code nécessaire à la virtualisation, pouvaient dans certaines circonstances générer de trop grandes trames, et engendrer, en combinaison avec d'autres facteurs, des plantages. La pile du noyau a été étendue à 16 ko pour tenir compte d'exigences plus élevées de certaines de ces fonctions.

On peut aussi remarquer la correction d'un problème dans le gestionnaire de ressources de mémoire : MEMCG pouvait conduire à des impasses dans des conditions OOM (out of memory). Ceci pouvait être utilisé pour bloquer le système, lorsque celui-ci est en condition OOM, en lançant de nombreux processus au sein d'un _cgroup_ restreint en mémoire.

L'utilitaire `perf` a été rétroporté dans sa dernière version, Red Hat 6.7 propose ainsi la même version 3.16 que le projet en amont, incluant toutes les améliorations et corrections des versions 3.13, .14 & .15. Les administrateurs & développeurs peuvent consulter le [« _Performance Tuning Guide_ »](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html-single/Performance_Tuning_Guide/index.html#s-analyzperf-perf) mis à jour.
