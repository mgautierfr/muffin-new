menu-position: 3
title:   Parlons XMPP - épisode 6 - les commandes à distance
id: xmpp
css-template: article.css
color1: #a07cbc
index-style: pure-u-1 pure-u-md-1-3
authors: Goffi
date:    2015-07-27T12:31:53+02:00
license: CC by-sa
tags:    xmpp, tutoriel et parlons_xmpp
---


(pour lire les épisodes précédents, suivez [l'étiquette correspondante](http://linuxfr.org/tags/parlons_xmpp/public))

Aujourd'hui nous allons parler d'une de mes fonctionnalités favorites dans XMPP : les commandes à distance. Il s'agit de la possibilité pour 2 entités XMPP d'exécuter des actions à distance de manière générique.

La première méthode, assez peu utilisée à ma connaissance, est via la [XEP-0009](https://xmpp.org/extensions/xep-0009.html) (oui c'est une vieille, elle date de 2001), qui donne une méthode pour utiliser [[XML-RPC]] à travers XMPP.
XML-RPC est un [Système de communications inter processus](https://fr.wikipedia.org/wiki/Communication_inter-processus), c'est à dire un moyen pour 2 entités de communiquer à distance. Vous pouvez ainsi exécuter des commandes indépendamment du langage de programmation, en ayant des données décrites (est-ce que je reçois un entier ? Une chaîne de caractères ?). Ceci est particulièrement adapté pour la communication de machines à machines, quand on sait déjà quelles méthodes sont disponibles et ce qu'elles font. Le cas type est une interface de programmation (ou [API](https://fr.wikipedia.org/wiki/Interface_de_programmation)) pour permettre de contrôler quelque chose, comme un serveur par exemple.

La deuxième méthode est appelée commandes « ad-hoc » (de circonstance), et c'est la [XEP-0050](https://xmpp.org/extensions/xep-0050.html) qui les explique. L'idée est aussi de pouvoir exécuter de manière générique des commandes, mais cette fois sans savoir à l'avance ce qui va être disponible, et en pensant plutôt à une interaction humain/machine, bien que machine/machine soit bien sûr tout à fait possible.

Le cas typique d'utilisation est la configuration d'un service, mais ce peut être utilisé pour n'importe quoi.
Ce qui rend cette fonctionnalité particulièrement puissante, c'est qu'elle fait partie de XMPP et donc peut profiter de tous ses avantages, en particulier l'authentification forte (quand on parle à `server@example.net`, on sait qu'on ne parle pas à quelqu'un d'autre) et la liste de contacts (roster) avec leurs groupes. Nous avons ainsi un système de permissions simple à mettre en œuvre et efficace.

Prenons un exemple: vous êtes administrateur sur Prosody (pour mémoire, un serveur XMPP populaire) – pour cela il vous suffit de donner votre jid dans la variable « admins » dans la configuration –, et vous voulez… pouvoir administrer votre serveur. Pas besoin de s'embêter à avoir une page web dédiée avec mot de passe etc, il vous suffit d'avoir un client XMPP qui gère les commandes ad-hoc (Gajim par exemple), et d'entrer l'adresse de votre serveur.

Exemple ci-dessous avec Libervia, l'adresse libervia.org est celle du serveur, voici ce que, en tant qu'admin, je peux voir.

![capture d'écran des commandes ad-hoc pour un admin avec Libervia](https://lut.im/HonhWxix/Tc4XYZxY)

Le « Send Announcement to Online Users » (envoyer une annonce à tous les utilisateurs en ligne) est particulièrement utile juste avant une mise à jour.

Avec un compte lambda, voilà ce que l'on voit :
![capture d'écran des commandes ad-hoc pour un utilisateur lambda avec Libervia](https://lut.im/Lu4nXhXC/Kq20qJbX)

Malheureusement avec certains clients, l'accès est tout sauf intuitif. Je pense que l'exemple le pire est Psi : pour accéder aux commandes du serveur il faut aller dans la découverte des services (« service discovery ») soit via le menu « General » soit en cliquant droit sur votre profil, puis cliquer sur le nom de votre serveur dans la liste des services, puis enfin cliquer sur l’icône en forme de terminal, appelée « execute command » (exécuter une commande).

Dans Gajim c'est plus simple (clique droit sur votre profil puis « exécuter une commande »), dans Swift c'est dans le menu Actions => Run Server Command (Exécuter une commande serveur). Si vous voulez exécuter des commandes sur un jid arbitraire dans Gajim, il vous faut passer par le menu « Découvrir les services », comme avec Psi ; je n'ai pas l'impression que ce soit possible avec Swift.

Ci dessous une capture de Movim, ces commandes sont dans le menu « actions »:

![les actions dans Movim](https://lut.im/VXeZ2Zvh/juPkxaB8)

Même sans être administrateur, les commandes « ad-hoc » peuvent vous être utiles. Par exemple, vous avez oublié de déconnecter votre client chez vous et vous vous trouvez chez un ami. Il vous suffit de vous connecter, et de spécifier le jid complet, avec la ressource, de votre client à la maison. SàT par exemple vous permet à l'heure actuelle de changer votre statut voire de vous déconnecter. Gajim permet en plus de transférer les messages non lus ou de quitter des salons de discussions.

![Accès à un client Gajim à distance](https://lut.im/eJsFAxZO/RDsH1pup)

Il est facile d'imaginer à quel point cette fonctionnalité peut être utile pour l'administration ou la surveillance de serveurs, pour la robotique, la domotique, le contrôle à distance de votre ordinateur, de votre bot XMPP, etc.

Voyons un autre cas pratique que nous avons implémenté dans Salut à Toi : on couple les commandes « ad-hoc » avec D-Bus. Pour ceux qui ne connaissent pas, D-Bus est un autre système de communication inter-processus qui a été développé par [[Freedesktop]] comme un standard commun dans les bureaux libres, en s'inspirant de l'existant et en particulier du [[DCOP]] de [[KDE]]. Il est donc très largement utilisé dans les logiciels libres, et permet de piloter beaucoup de logiciels courants.
En liant D-Bus et ad-hoc, SàT permet de créer très facilement une télécommande universelle, pilotable depuis n'importe quel client XMPP compatible (y compris sur votre téléphone, ce qui est particulièrement intéressant pour une télécommande), et en prime avec gestion des permissions par groupe.



Cette fonctionnalité est montrée dans le courte vidéo ci-dessous, on autorise les membres du groupe « coloc » à piloter une instance de VLC, pratique quand on fait une séance cinéma dans la colocation :).

Cliquez sur l'image ci-dessous pour voir la vidéo:
[![Télécommande universelle dans Salut à Toi](http://ftp.goffi.org/media/screencasts/posters/présentation_SàT_7_télécommande_universelle.png)](http://ftp.goffi.org/media/screencasts/pr%C3%A9sentation_S%C3%A0T_7_t%C3%A9l%C3%A9commande_universelle.webm)

Nous allons ajouter également la possibilité d’exécuter des scripts ou des commandes shell, on pourra ainsi très facilement autoriser tous les administrateurs d'un serveur à le redémarrer ou à avoir son statut.

Petite note sur le fonctionnement : les commandes ad-hoc, comme à peu près tout ce qui demande de la transmission d'informations typées dans XMPP, se basent sur les « Data Forms » (formulaires de données, [XEP-0004](https://xmpp.org/extensions/xep-0004.html)). Cette extension standardise donc les formulaires, et permet de demander des booléens, des mots de passes ou encore des listes de jids.

J'en profite aussi pour vous donner [le lien vers la conférence « PubSub, microblogage et XMPP »](https://rmll.ubicast.tv/videos/pubsub-microblogage-et-xmpp/) que j'ai faite aux RMLL il y a un peu plus de 2 semaines. J'y explique en 20 min les bases de PubSub et du microblogage dans XMPP, ainsi que l'intérêt des 2 XEPs que nous avons publiées (cf [ce journal](https://linuxfr.org/users/goffi/journaux/xmpp-et-micro-blogage-la-donne-a-change)). J'en reparlerai bien sûr dans un prochain article.

Bien qu'ayant plusieurs idées en tête, je ne suis pas encore décidé pour le prochain article, il est possible que je parle de PubSub, de copie de fichiers, de chiffrement de bout en bout, ou de tout autre chose…

