import os
MKD_PATT = r'\.(?:css)$'

class CSSPage(dict):
    """Virtual CSS page to let poole generate the custom css page"""

    def __init__(self, fname, original_page):
        super(CSSPage, self).__init__()

        self.update(original_page)

        del self["menu-position"]

        fname = os.path.join(project, fname)
        self["template"] = fname

        self["output"] = fname[:-4] + self['id'] + fname[-4:]

        #import pdb; pdb.set_trace()
        self["url"] = self["output"][len(project):].lstrip(os.path.sep)
        self["url"] = self["url"].replace(os.path.sep, "/")

        self["output"] = self["output"].replace(project, output)

        with open(os.path.join(project,fname), 'r', encoding="utf8") as fp:
            self.raw = fp.readlines()

        self.html = ""

        original_page["css-url"] = self["url"]

    def __getattr__(self, name):
        """Attribute-style access to dictionary items."""
        try:
            return self[name]
        except KeyError:
            raise AttributeError(name)


def hook_postconvert_make_css():
    """Create virtual page than will generate css"""

    new_pages = []
    for p in pages:
        if 'css-url' not in p:
            p['css-url'] = 'article.css'

        if 'css-template' in p:
            new_pages.append(CSSPage(p['css-template'], p))

    pages.extend(new_pages)
        
